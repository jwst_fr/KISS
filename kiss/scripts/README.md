# Purpose
The directory scripts of KISS (Keep It Simple Simulator) collects
 python script to check, plot KISS agains the offcial simulator 
 product and against PASP paper IX

# How to run
* script_offical_simulator_5stars.py
It calls the official simulator (which should be installed) and create the FITS file reference_5stars.fits, 4.2 MegaBytes, an illumination model (electron/second, no noise, no pixel flat)

* script_make_catalog_5stars.py
It creates the file catalog_5stars.dat, the catalog of the 5 stars corresponding to the script official simulator.

* config_5stars.ini
to be called by launch_kiss
python launch_kisse scripts/config_5stars.ini
A FITS file illumination_5stars.fits is created

* global_check.py
 compare illumination_5stars.fits  and reference_5stars.fits position of the stars, flux of the stars and background

* compute_distortion_polynom_coef_radec
It reads a file containting a grid of points with coordinates both on the sky (not distorted) and on the cameara (distorted) and compute the two 2D poynoms of degree 5 for the distortion.

* xlsx2fits.py
  A script which reads the excel file of Alistair Glasse, catalog of stars in the Large Magellanic Cloud,
containing stard id, ra, dec, flux at K, and flux in the different filters of MIRIM.

* Polywarp
the equivalent of IDL poylwarp.
polywarp uses least squares estimation to determine the coefficients of the two 2D polynoms, Kx and ky
    Xi = sum over i and j of:  Kx[i,j] * Xo^j * Yo^i
    Yi = sum over i and j of:  Ky[i,j] * Xo^j * Yo^i
