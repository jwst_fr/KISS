#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 12 March 2018  KISS package
#  author R Gastaud

import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits
from scipy.ndimage.measurements import center_of_mass


def check_one_star(i, mirisim_image, kiss_image, delta, bkg, verbose=True, plotting=True):
    """

    :param i:
    :type i:
    :param mirisim_image:
    :type mirisim_image:
    :param kiss_image:
    :type kiss_image:
    :param delta:
    :type delta:
    :param bkg:
    :type bkg:
    :param verbose:
    :type verbose:
    :param plotting:
    :type plotting:
    :return:
    :rtype:
    """
    (y_o, x_o) = np.unravel_index(mirisim_image.argmax(), mirisim_image.shape)
    (y_k, x_k) = np.unravel_index(kiss_image.argmax(), kiss_image.shape)
    print(" maximum position mirisim x_o={:g} y_o={:g};  kiss x_k={:g} y_k={:g}".format(x_o, y_o, x_k, y_k))
    ##  imagette
    im_o = mirisim_image[y_o - delta:y_o + delta, x_o - delta:x_o + delta]
    im_k = kiss_image[y_o - delta:y_o + delta, x_o - delta:x_o + delta]
    #
    (y_ob, x_ob) = center_of_mass(im_o)
    (y_kb, x_kb) = center_of_mass(im_k)
    x_ob += x_o - delta
    y_ob += y_o - delta
    x_kb += x_k - delta
    y_kb += y_k - delta
    print(" barycenter mirisim x_ob={:g} y_ob={:g}; kiss x_kb={:g} y_kb={:g}".format(x_ob, y_ob, x_kb, y_kb))

    if plotting:
        plt.figure(i)
        plt.title('im_o - im_k ' + str(i))
        plt.imshow(im_o - im_k, origin='lower')
        # ax.set_title('im_o - im_k '+str(i))
        plt.colorbar()

    if verbose:
        print("corner", i)
        print ('minmax mirisim', im_o.min(), im_o.max())
        print ('minmax kiss', im_k.min(), im_k.max())
    #
    ##  flux
    n_pixels = im_o.size
    flux_o = im_o.sum() - bkg * n_pixels
    flux_k = im_k.sum() - bkg * n_pixels
    rdiff = (flux_k - flux_o) / flux_o * 100
    print(" flux mirisim = {:g} e-/s ; kiss = {:g} e-/s ; rdiff = {:.3g}%".format(flux_o, flux_k, rdiff))
    # print(" position mirisim = x_o{:}, y_o{:}; kiss =  x_k{:}, y_k{:}".format(x_o, y_o, x_k, y_k))
    #
    mirisim_image[y_o - delta:y_o + delta, x_o - delta:x_o + delta] = bkg
    kiss_image[y_o - delta:y_o + delta, x_o - delta:x_o + delta] = bkg
    return flux_k, flux_o


def check(mirisim_file, kiss_file, plotting=True, delta=16):
    """

    :param mirisim_file:
    :type mirisim_file:
    :param kiss_file:
    :type kiss_file:
    :param plotting:
    :type plotting:
    :param delta:
    :type delta:
    :return:
    :rtype:
    """
    ###########################
    ### read the fits file
    hdulist1 = fits.open(mirisim_file)
    mirisim_image = hdulist1['INTENSITY'].data.squeeze()

    hdulist2 = fits.open(kiss_file)
    kiss_image = hdulist2[0].data
    kiss_image.min(), kiss_image.max()

    if plotting:
        plt.figure(1)
        plt.title('mirisim')
        plt.imshow(np.log(mirisim_image + 1), origin='lower')
        plt.figure(2)
        plt.title('kiss')
        plt.imshow(np.log(kiss_image + 1), origin='lower')
        print ('minmax mirisim', mirisim_image.min(), mirisim_image.max())
        print ('minmax kiss', kiss_image.min(), kiss_image.max())

    ###########################
    ### compare the background
    bkg_o = np.median(mirisim_image)
    bkg_k = np.median(kiss_image)
    rdiff = (bkg_k - bkg_o) / bkg_o * 100
    print(" background mirisim = {:g} e-/s ; kiss = {:g} e-/s ; rdiff = {:.3g}%".format(bkg_o, bkg_k, rdiff))
    # rdiff = -0.0339%
    ##########################
    ### compute flux and position for each star
    fluxes_k = np.zeros(5)
    fluxes_o = np.zeros(5)
    for i in np.arange(5):
        print(i + 2)
        # check_one_star(2+i, mirisim_image, kiss_image, delta, bkg_o, verbose=True, plotting=True)
        fk, fo = check_one_star(2 + i, mirisim_image, kiss_image, delta, bkg_o, verbose=False, plotting=False)
        fluxes_k[i] = fk
        fluxes_o[i] = fo
    #
    ##########################
    ###  examine the fluxes
    x = 5. - np.arange(5)  # original fluxes in milliJansky
    #
    b_k, a_k = np.polyfit(x, fluxes_k, 1)
    y_k = a_k + b_k * x
    #
    b_m, a_m = np.polyfit(x, fluxes_o, 1)
    y_o = a_m + b_m * x
    #
    print("kiss flux fit: a_k= {:}, b_k={:} ; mirisim fit flux: a_m= {:}, b_m={:}, ratio={:}".format(a_k, b_k, a_m, b_m, b_k/b_m))
    #
    plt.figure(10)
    plt.title('flux for delta=' + str(int(delta)))
    plt.xlabel('input flux milliJansky')
    plt.ylabel('measured flux e/s')
    plt.plot(x, fluxes_k, 'g+', label='kiss')
    plt.plot(x, y_k, 'g', label='fitted kiss ')
    plt.plot(x, fluxes_o, 'b+', label='mirisim')
    plt.plot(x, y_o, 'b', label='fitted mirisim')
    plt.legend()
    #
    plt.figure(11)
    plt.title('residu of line fit of flux for delta=' + str(int(delta)))
    plt.xlabel('input flux milliJansky')
    plt.ylabel('residu on measured flux e/s')
    plt.plot(x, fluxes_k - y_k, 'g', label='kiss')
    plt.plot(x, fluxes_o - y_o, 'b', label='mirisim')
    plt.legend()
    return


# The following code is for development and testing only
if __name__ == '__main__':
    mirisim_file = 'output/mirisim_5stars.fits'
    mirisim_file = 'mirisim_5stars.fits'
    kiss_file = 'output/illumination0_5stars_illum_1.fits'
    PLOTTING = True
    delta = 512 / 4 / 2
    delta = 16
    check(mirisim_file, kiss_file, plotting=PLOTTING, delta=delta)
