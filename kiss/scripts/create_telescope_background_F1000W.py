# Create a dummy telescope background
#  RG 16 June 2020

#################  Modeling with Gaussian2D ####################
from astropy.modeling import models, fitting
from astropy.modeling.functional_models import Gaussian2D
from astropy.modeling.functional_models import Moffat2D
from astropy.modeling.functional_models import Ellipse2D
from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
#
nx=1024
ny=1024
#
y, x = np.mgrid[:ny, :nx]
gg = Gaussian2D(x_mean=600, y_mean=400, x_stddev=100, y_stddev=200, theta=30, amplitude=100.)
image = gg(x, y)
plt.imshow(image, origin='lower')
image  = image+100
hdu = fits.PrimaryHDU(image)
hdu.header
hdu.header['BUNIT']='electron/second'
hdu.header['FILTER']='F1000W'
hdu.header['FILENAME']='TELESCOPE_BACKGROUND'
hdu.writeto('telescope_background_F1000W.fits')
