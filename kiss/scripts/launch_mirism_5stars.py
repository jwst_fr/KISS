#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    This script belongs to the KISS package.
    It calls the official simulator to create a Miri Illumination Model.
    An illumination model is the image in electron/second, before the call of SCASIM.
    The scene include background, 5 stars, one at the center of the detector, 
    the 4 others at each corner  with spacing delta.
    The output is the fits file  reference_5stars.fit.
    
    :History:
    03 March 2018: Created.
    
    @author:  R Gastaud (CEA Saclay)
    """

# 3 March 2018  KISS package
#  author R Gastaud
#
##  check to test the flux
#   simplification of test_mirimImager.py
#
from __future__ import print_function
import numpy as np
import astropy.io.fits as fits
#
from mirisim.imsim.mirimImager import MirimImager
from mirisim.config_parser import SimulatorConfig
from mirisim.imsim.tests.make_dummy_pointing import make_dummy_pointing
from mirisim.skysim import Point, Background, sed
from mirisim.imsim.mirimImager import __version__

print("version {}".format(__version__))

filter_name = 'F1000W'
cfgpath = 'IMA_FULL'
out_file = 'mirisim_5stars.fits'
background = Background(level='high')

flux_ref = 1e3  ### microJansky  #  saturation at 16 milliJansky
wref = 10.  # reference wavelength in micron
temperature = 4300.  # unit :Kelvin, so that at 10 micron we are in the Rayleigh Jeans approximation

# create 4 point source
delta = 30
x = np.array([0, delta, -delta, -delta, delta])
y = np.array([0, delta, delta, -delta, -delta])
print("x=", x)
print("y=", y)

my_sky = background
# put the brightest source at the center i=0, x=0,y=0  flux = 6*ref
for i in np.arange(5):
    starsed = sed.BBSed(temperature, flux=(5. - i) * flux_ref, wref=wref)  # descending order for the flux
    star = Point(Cen=(x[i], y[i]), vel=0.)
    star.set_SED(starsed)
    my_sky = my_sky + star
#
pointing = make_dummy_pointing(cfgpath, filter_name)
simulator_config = SimulatorConfig.from_default()
#
illumination = MirimImager(my_sky, cfgpath, filter_name, pointing,
                           rel_obsdate=0.0,
                           simulator_config=simulator_config,
                           verbose=True, debug=True)
#
image = illumination.intensity.squeeze()
print("write ", out_file)
illumination.write(out_file)
