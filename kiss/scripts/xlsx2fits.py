#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 2 March 2018  KISS package
#  author R Gastaud
#
#  where to get the excel file :
# http://miri.ster.kuleuven.be/bin/view/Internal/CAR-007?validation_key=e0035bc265330f1a3a901128e2c6f0e3
#
#
# how to get the xlrd to read  excel files
# conda install -c conda-forge xlrd
#
#  how to read  excel files
# https://www.datacamp.com/community/tutorials/python-excel-tutorial
#
from __future__ import print_function
import pandas as pd
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import os

#  BEWARE hard-coded, skip the first 3 lines
#              skip one column
#
#  input file
in_file = '../../big_data/JA_LMC_MIRI_v2.xlsx'
#  output file
out_file = 'JA_LMC_MIRI_v2.fits'

if not (os.path.exists(in_file)):
    print(in_file + " does not exist")

xl = pd.ExcelFile(in_file)
print(xl.sheet_names)
df1 = xl.parse('MASTER.ISO - Copy')
print(')df1.keys()', df1.keys())

# I try guess the unnamed keys
names = ['JA ID', 'ra', 'dec', 'kuse', '-kuse/2.5', 'F560W', 'F770W', 'F1000W', 'F1130W', 'F1280W', 'F1500W',
         'F1800W', 'F2100W', 'F2550W']

nf = 14  # all fluxes
dnames = df1.keys()

#  what have we ?
#  whe have to remove the first 3 lines
#  see script_read_catalog

#  what have we ?
plt.figure(1)
plt.plot(df1['ra'].values, df1['dec'].values, 'b+')

# remove the first 3 lines
plt.figure(2)
plt.plot(df1['ra'].values[3:], df1['dec'].values[3:], 'g+')

flux0 = df1['kuse'].values[3:] * u.STmag
# what is '-kuse/2.5'
plt.figure(3)
plt.plot(flux0, df1['-kuse/2.5'].values[3:], 'r+')
coefs = np.polynomial.polynomial.polyfit(flux0.value, df1['-kuse/2.5'].values[3:], 1)
# -1.36146223e-13,  -4.00000000e-01
#  and 0.4 = 1/2.5
#  ==>  '-kuse/2.5' = -(0.4)*kuse = kuse/(-2.5)

fluxK = flux0.to(u.milliJansky, u.spectral_density(2.2 * u.micron))
plt.figure(3)
plt.semilogy(flux0, fluxK)
plt.xlabel('magnitude K')
plt.ylabel('milliJansky')

ja_id = df1['JA ID'].values[3:].astype(int)
print(ja_id.min(), ja_id.max())
my_id = ja_id.astype(str)
#
#######################
#  skip one column  ==> i+1
for i in np.arange(5, nf):
    filter = names[i]
    wave = float(filter[1:-1]) / 100.
    flux = df1[dnames[i + 1]].values[3:]
    ratio = np.median(fluxK.value / flux)
    factor = np.sqrt(ratio) * 2.2 / wave
    print(i, names[i], dnames[i + 1], wave, 'factor=', factor)

####
## write the fits

col1 = fits.Column(name='id', array=my_id, format='7A')
liste = [col1]

for i in np.arange(2):
    print(i + 1, dnames[i + 1])
    col = fits.Column(name=dnames[i + 1], format='E', unit='deg', array=df1[dnames[i + 1]].values[3:])
    liste.append(col)
print(' id, ra, dec', liste)

for i in np.arange(3, 5):
    print(i, names[i], dnames[i])
    col = fits.Column(name=names[i], format='E', unit='magnitude', array=df1[dnames[i]].values[3:])
    liste.append(col)

for i in np.arange(5, nf):  # bug i+1
    print(i, names[i], dnames[i + 1])
    col = fits.Column(name=names[i], format='E', unit='milliJansky', array=df1[dnames[i + 1]].values[3:])
    liste.append(col)

print('final liste ', liste)
#
print("write")
hdu = fits.BinTableHDU.from_columns(liste)  #
hdu.writeto(out_file)
print(out_file, "written")
