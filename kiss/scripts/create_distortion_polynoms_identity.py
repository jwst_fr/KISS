# create_distortion_polynoms_identity
##  see compute_distortion_polynom_coef_radec.py

import matplotlib.pyplot as plt
import numpy as np
import datetime
from astropy.io import fits

filename = 'distortion_polynoms_identity.fits'
crpix1 = 688.5
crpix2 = 511.5 ## reference pixel
crota2 = -4.45 # degree, not used

# create the 4 2D polynoms
A = np.zeros([5,5])
B = np.zeros([5,5])
AP = np.zeros([5,5])
BP = np.zeros([5,5])
A[1,0]  = 1
B[0,1]  = 1
AP[1,0] = 1
BP[0,1] = 1

# create a record
data = np.rec.array(
    [
        (A.flatten(),
         B.flatten(),
         AP.flatten(),
         BP.flatten())
    ], formats='25f8,25f8,25f8,25f8', names='A,B,Ap,Bp')

print(data.shape)

### now create the fits header
hdu = fits.BinTableHDU.from_columns(data)
hdu.header['TDIM1'] = '(5,5)'
hdu.header['TDIM2'] = '(5,5)'
hdu.header['TDIM3'] = '(5,5)'
hdu.header['TDIM4'] = '(5,5)'
hdu.header['CRPIX1'] = crpix1
hdu.header['CRPIX2'] = crpix2
hdu.header['CROTA2'] = crota2

format = '%Y-%m-%d-%H-%M-%S'
hdu.header['date'] = datetime.datetime.now().strftime(format)
hdu.header['version'] = 0
hdu.header['history'] = 'IDENTITY = No Distortion'

#import kiss
#full_filename = pkg_resources.resource_filename('kiss', filename)
#hdu.writeto(full_filename)
hdu.writeto(filename)

########  Check the Identity Distortion
from kiss.steps import ComputeDistortion

nx = 1024
ny = 1024

distortion_object = ComputeDistortion(filename)

y_0, x_0 = np.mgrid[:ny, :nx]
x_1, y_1 = distortion_object.foc2pix(x_0.flatten(), y_0.flatten())
x_1 = x_1.reshape([ny,nx])
y_1 = y_1.reshape([ny,nx])
diff_y =  y_0 - y_1
diff_x =  x_0 - x_1
print('diff_x', diff_x.min(), diff_x.max())
print('diff_y', diff_y.min(), diff_y.max())

x_1, y_1 = distortion_object.pix2foc(x_0.flatten(), y_0.flatten())
x_1 = x_1.reshape([ny,nx])
y_1 = y_1.reshape([ny,nx])
diff_y =  y_0 - y_1
diff_x =  x_0 - x_1
print('diff_x', diff_x.min(), diff_x.max())
print('diff_y', diff_y.min(), diff_y.max())


