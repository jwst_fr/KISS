#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 12 March 2018  KISS package
#  author R Gastaud

from astropy.io import fits
import numpy as np
import numpy.testing as npt
from astropy import wcs
from astropy import units as u

from .Polywarp import polywarp


def read_distortion_reference_point(filename):
    """
    It reads a FITS file containing a set of points (grid) with coordinates in
    (RA, DEC)   :not distorted  unit given by the keyword tunit, can be 'deg', 'arscsec', etc...
    (col, row) : distorted   unit : pixel

    :param filename: File containing EFP and DFP reference coordinates
    :type filename: str

    :return: ra, dec in decimal degree, col, row in pixel
    :rtype: Union[nd.array, nd.array, nd.array, nd.array]
    """

    #  read the grid
    hdulist = fits.open(filename)
    ra = hdulist[1].data['ra'] * u.Unit(hdulist[1].columns['ra'].unit)
    ra = ra.to(u.deg).value
    dec = hdulist[1].data['dec'] * u.Unit(hdulist[1].columns['dec'].unit)
    dec = dec.to(u.deg).value
    col = hdulist[1].data['col']
    row = hdulist[1].data['row']
    return ra, dec, col, row


def check_wcs(col, row, ra, dec, crpix, cdelt, pa, rtol=0.02, atol=1, verbose=True):
    """
    The aim of this program is to check that a simple object w.wcs, with the default values of its parameters cdelt, crpix, pa is enough to compute the non distorted part of the world 2 pixel transform.
    The distortion will take care of the residue of this affine transform.
    For this I compute with polywarp two polynoms 2d, both of degree 1, to fit 
       (col, row)  to (ra, dec)
       
    :param col: column of the projection of the points of the grid on the camera, unit pixel
    :type col: numpy array of floats, shape npoints

    :param row: row of the projection of the points of the grid on the camera , unit pixel
    :type row: numpy array of floats, shape npoints


    :param ra: right ascension of the points of the grid in the sky, unit decimal degree
    :type ra:  numpy array of floats, shape npoints

    :param dec: declination  of the points of the grid in the sky, unit decimal degree
    :type dec:  numpy array of floats, shape npoints

    :param cdelt: size of the detector pixel projected in sky, unit arsecond
    :type cdelt: numpy array of floats, shape=2

    :param pa: position angle, angle of the second axis of the detector with North, unit decimal degree
    :type pa: float

    :param crpix: the coordinates of the reference pixel in the detector (unit pixel)
    e.g. np.array([688.5, 511.5]) or np.array([511.5, 511.5])
    :type crpix:  numpy array of two floats

    :param rtol: relative tolerance for the assert
    :type rtol: float

    :param atol: absolute tolerance for the assert
    :type atol: float

    :param verbose: flag to get some information ...
    :type verbose: bool
   """
    kx, ky = polywarp(col, row, ra * 3600, dec * 3600)
    # measured shift
    ccrpix = np.array([kx[0, 0], ky[0, 0]])
    npt.assert_allclose(crpix, ccrpix, atol=atol)
    if verbose:
        print(" measured crpix = {} {}".format(ccrpix[0], ccrpix[1]))

    # measured magnification
    matrix = np.array([[kx[1, 0], kx[0, 1]], [ky[1, 0], ky[0, 1]]])
    gx = np.abs(kx[1, 0])
    gy = np.abs(ky[0, 1])
    gxy = np.sqrt(np.abs(np.linalg.det(matrix)))
    
    if verbose:
        print(" magnification {} {} {}", gx, gy, gxy)
    npt.assert_allclose(gx, 1 / np.abs(cdelt[0]), rtol=rtol)
    npt.assert_allclose(gy, 1 / np.abs(cdelt[1]), rtol=rtol)

    # measured angle
    angle1 = np.arctan2(np.abs(matrix[0, 1]), np.abs(matrix[0, 0]))
    angle2 = np.arctan2(np.abs(matrix[1, 0]), np.abs(matrix[1, 1]))
    angle1 = np.degrees(angle1)
    angle2 = np.degrees(angle2)
    if verbose:
        print(" angle {} {} {}", pa, angle1, angle2)
    return


def world2pix_centered(ra, dec, col, row, crpix, cdelt, pa=0):
    """
    This programs prepare the data to the comput of the distortion.
    The return coordinates are centered on the reference camera point (crpix) and expressed in camera coordinates.
    It tansforms  the coordinates of the points of the grid in the camera (row, col) and center them
    So (ra,dec) is transformed into undistorded pixel coordinates.
    For this create a wcs with crpix, pa, cdelt. Then they are centered
    It also centered (col, row), the measured position on the dectector, with reference to crpix
    
    :param ra: right ascension of the points of the grid in the sky, unit decimal degree
    :type ra:  numpy array of floats, shape npoints
    
    :param dec: declination  of the points of the grid in the sky, unit decimal degree
    :type dec:  numpy array of floats, shape npoints
    
    :param col: column of the projection of the points of the grid on the camera, unit pixel
    :type col: numpy array of floats, shape npoints
    
    :param row: row of the projection of the points of the grid on the camera , unit pixel
    :type row: numpy array of floats, shape npoints
    
    :param crpix: the coordinates of the reference pixel in the detector (unit pixel)
                 e.g. np.array([688.5, 511.5]) or np.array([511.5, 511.5])
    :type crpix:  numpy array of two floats
    
    :param pa: position angle, angle of the second axis of the detector with North, unit decimal degree
    :type pa: float
    
    :param cdelt: size of the detector pixel projected in sky, unit arsecond, default value 0.11
    :type cdelt: numpy array of floats, shape=2

    :type pa: float
    
    :return: (perfect_x, perfect_y, distorted_x, distorted_y, w)
        perfect_x: x coordinate of the projection of the points of the grid on the camera, centered, unit pixel
        perfect_y: y coordinate of the projection of the points of the grid on the camera, centered, unit pixel
        distorted_x: x coordinate of the projection of the points of the grid on the camera, centered, distorted, unit pixel
        distorted_y: y coordinate of the projection of the points of the grid on the camera, centered, distorted, unit pixel
        w: object to transform the coordinates world 2 pixel and pixel 2 world
    :rtype: Union[nd.array(float), nd.array(float), nd.array(float), nd.array(float), astropy.wcs.WCS]
    """

    w = wcs.WCS(naxis=2)
    w.wcs.ctype = ["RA---TAN", "DEC--TAN"]
    w.wcs.cdelt = cdelt / 3600.
    w.wcs.crpix = crpix
    w.wcs.crota = [pa] * 2  # <=> [pa, pa]

    ccol, rrow = w.wcs_world2pix(ra, dec, 1)

    perfect_x = ccol - crpix[0]
    perfect_y = rrow - crpix[1]
    distorted_x = col - crpix[0]
    distorted_y = row - crpix[1]
    return perfect_x, perfect_y, distorted_x, distorted_y, w


def compute_distortion_polynom_coef_radec(grid_file, poly_file, crpix, cdelt, pa=0):
    """
    This programs reads a file (grid_file) which contains the coordinates of a set of points
    both in the sky (ra,dec), not distorted and their projection in the detector (col, row)
    It computes then the distortion polynoms using polywap.
    
    It is advised to check the result with kiss.tests.test_distortion_radec
    
    :param grid_file: the name of the grid file.
    :type grid_file: string
    
    :param poly_file: the name of the file where the result (two 2D polynoms of degree 5) will be stored
    :type poly_file: string
    
    :param crpix: the coordinates of the reference pixel in the detector (unit pixel)
    e.g. np.array([688.5, 511.5]) or np.array([511.5, 511.5])
    :type crpix:  numpy array of two floats

    :param cdelt: size of the detector pixel projected in sky, unit arsecond
    :type cdelt: numpy array of floats, shape=2

    :param pa: position angle, angle of the second axis of the detector with North, unit decimal degree
    :type pa: float


    """

    #  read the grid  ra dec in degree
    ra, dec, col, row = read_distortion_reference_point(grid_file)

    check_wcs(col, row, ra, dec, crpix, cdelt, pa)

    perfect_x, perfect_y, distorted_x, distorted_y, w = world2pix_centered(ra, dec, col, row, crpix, cdelt, pa)

    kx, ky = polywarp(distorted_x, distorted_y, perfect_x, perfect_y, 4)
    kxp, kyp = polywarp(perfect_x, perfect_y, distorted_x, distorted_y, 4)

    data = np.rec.array(
        [
            (kx.flatten(),
             ky.flatten(),
             kxp.flatten(),
             kyp.flatten())
        ], formats='25f8,25f8,25f8,25f8', names='A,B,Ap,Bp')

    hdu = fits.BinTableHDU.from_columns(data)
    hdu.header['TDIM1'] = '(5,5)'
    hdu.header['TDIM2'] = '(5,5)'
    hdu.header['TDIM3'] = '(5,5)'
    hdu.header['TDIM4'] = '(5,5)'

    hdu.header['CRPIX1'] = w.wcs.crpix[0]
    hdu.header['CRPIX2'] = w.wcs.crpix[1]
    hdu.header['CROTA2'] = w.wcs.crota[0]
    import datetime
    format = '%Y-%m-%d-%H-%M-%S'
    hdu.header['date'] = datetime.datetime.now().strftime(format)
    hdu.header['version'] = 5
    hdu.header['history'] = 'compute from the centered pseudo pixels and col, row centered'

    hdu.writeto(poly_file)


#
# The following code is for development and testing only
#
if __name__ == '__main__':
    print("Testing  kiss routine")
    PLOTTING = True  # Set to False to turn off plotting.
    grid_file = '../tests/MIRIM_grid_radec.fits'
    poly_file = 'distortion_polynoms_mirisim.fits'
    pa = -4.45
    cdelt = np.array([-0.11, 0.11])
    crpix = np.array([688.5, 511.5])
    compute_distortion_polynom_coef_radec(grid_file, poly_file, crpix=crpix, cdelt=cdelt, pa=pa)
    #

    print("\nTest finished.")
