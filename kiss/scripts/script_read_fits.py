#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 2 March 2018  KISS package
#  author R Gastaud

from astropy.io import fits
from astropy import units as u
import matplotlib.pyplot as plt

#
filename = 'rg_lmc.fits'
hdulist = fits.open(filename)

ja_id = hdulist[1].data['ja_id']
ra = hdulist[1].data['ra'] * u.Unit(hdulist[1].columns['ra'].unit)
dec = hdulist[1].data['dec'] * u.Unit(hdulist[1].columns['dec'].unit)

plt.plot(ra, dec, '+')
names = ['JA ID', 'ra', 'dec', 'kuse', '-kuse/2.5', 'F560W', 'F770W', 'F1000W', 'F1130W', 'F1280W', 'F1500W',
         'F1800W', 'F2100W', 'F2550W']
filter_name = 'kuse'
flux0 = hdulist[1].data[filter_name] * u.Unit(hdulist[1].columns[filter_name].unit)
# filter_name = 'F2100W'
# f2100 = hdulist[1].data[filter_name]*u.Unit(hdulist[1].columns[filter_name].unit)
# print(f2100.min() f2100.max())
# (<Quantity 7.06577552023191e-08 mJy>, <Quantity 6.735344886779785 mJy>)
# plt.plot(f2100)
