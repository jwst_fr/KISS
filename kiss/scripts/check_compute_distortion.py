from __future__ import print_function

"""
Created on 08 March 2018
@author: Rene Gastaud

history : 

"""

from kiss.steps.compute_distortion import ComputeDistortion
from read_perfect_distort_xy import read_perfect_distort_xy

__version__ = 0


def check_compute_distortion(poly_file, grid_file):
    print("Testing the comput distortion  class.")
    cd = ComputeDistortion(poly_file)
    print("cd=", cd)
    perfect_x, perfect_y, distorted_x, distorted_y = read_perfect_distort_xy(grid_file, verbose=True)
    #
    corrected_x, corrected_y = cd.pix2foc(distorted_x, distorted_y)
    diff_x = corrected_x - perfect_x
    print("corrected_x - perfect_x ", diff_x.min(), diff_x.max())
    diff_y = corrected_y - perfect_y
    print("corrected_y - perfect_y ", diff_y.min(), diff_y.max())
    #
    ddistort_x, ddistort_y = cd.foc2pix(perfect_x, perfect_y)
    diff_x = ddistort_x - distorted_x
    print("corrected_x - ddistort_x ", diff_x.min(), diff_x.max())
    diff_y = ddistort_y - distorted_y
    print("corrected_y - ddistort_y ", diff_y.min(), diff_y.max())


if __name__ == '__main__':
    poly_file = 'distortion_polynoms.fits'
    grid_file = 'MIRIM_grid_imaging.fits'
    print("check compute_distortion ", __version__)
    check_compute_distortion(poly_file, grid_file)
