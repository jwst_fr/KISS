#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create a PSF CDP FITS file from webbpsf output file.
# Rene Gastaud,  February 22nd 2021 
# /Users/gastaud/scratch/CARs/car_lmc/KISS

from astropy.io import fits
import numpy as np
from astropy.table import Table, Column

#######################  to be customised #############################
webb_filename = 'Lyot_coronagraph_24p_0d.fits'
col_field = [24]
row_field = [0]
psf_filename = 'psf_Lyot_coronagraph_24p_0d.fits'
######################### end of customisation ##################

###  read webbpsf result
hdul_webb = fits.open('Lyot_coronagraph_24p_0d.fits')
hdul_webb.info()
image = hdul_webb['OVERSAMP'].data
det_samp = hdul_webb[0].header['DET_SAMP']
hdul_webb.close

###  write "psf" for KISS
col1=fits.Column(name='COL_FIELD', format='D', array=np.array(col_field), unit='camera pixel')
col2=fits.Column(name='ROW_FIELD', format='D', array=np.array(row_field), unit='camera pixel')
new_cols = fits.ColDefs([col1, col2])
hdu_lut = fits.BinTableHDU.from_columns(new_cols, name='psf_lut')

sci_hdu = fits.ImageHDU(image, name="SCI")
phdu = fits.PrimaryHDU()
phdu.header['DET_SAMP'] = det_samp
pixel_size = 25/det_samp
phdu.header['PIXSIZE'] = pixel_size

hdulist = fits.HDUList([phdu, sci_hdu, hdu_lut])
hdulist.writeto(psf_filename, overwrite=True)
hdulist.close()

"""
hdul = fits.open(psf_filename)
hdul.info()
"""


