# IPython log file

import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits
from scipy.ndimage.measurements import center_of_mass
#

filename1 = 'output/illumination0_5stars_illum_1.fits'
filename2= 'output/illumination00_5stars_illum_1.fits'
#
hdulist1 = fits.open(filename1)
image1 = hdulist1[0].data
transmission1 =  hdulist1[0].header['transmission']
hdulist1.close()
#
hdulist2 = fits.open(filename2)
image2 = hdulist2[0].data
transmission2 = hdulist2[0].header['transmission']
hdulist2.close()
#

print('image2', transmission2, image2.shape, image2.min(), image2.max())
print('image1', transmission1, image1.shape, image1.min(), image1.max())
diff = image2-image1
print('diff', diff.min(), diff.mean(), diff.max())
#
plt.imshow(diff, origin='lower')
#hdu = fits.PrimaryHDU(diff)
#hdu.writeto('diff.fits')
