#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 08 March  KISS package
#  author R Gastaud


from astropy.io import fits


def read_perfect_distort_xy(filename, verbose=False):
    # 'MIRIM_grid_imaging.fits'
    hdulist1 = fits.open(filename)
    efp_x = hdulist1[1].data['EFP_X'] / 1000.
    efp_y = hdulist1[1].data['EFP_Y'] / 1000.
    dfp_x = hdulist1[1].data['DFP_X'] / 1000.
    dfp_y = hdulist1[1].data['DFP_Y'] / 1000.
    hdulist1.close()
    gx = 0.35163359841036418
    gy = 0.3543513074369713
    #
    efp_x = efp_x.squeeze()
    efp_y = efp_y.squeeze()
    dfp_y = dfp_y.squeeze()
    dfp_x = dfp_x.squeeze()
    #
    distorted_x = dfp_x * 40 + 511.5
    distorted_y = dfp_y * 40 + 511.5
    perfect_x = efp_x * 40 * gx + 511.5
    perfect_y = efp_y * 40 * gy + 511.5
    #
    diff_x = distorted_x - perfect_x
    if verbose:
        print ("diff_x before", diff_x.min(), diff_x.max())

    diff_y = distorted_y - perfect_y
    if verbose:
        print ("diff_y before", diff_y.min(), diff_y.max())

    return perfect_x, perfect_y, distorted_x, distorted_y


# The following code is for development and testing only
if __name__ == '__main__':
    filename = 'MIRIM_grid_imaging.fits'
    perfect_x, perfect_y, distorted_x, distorted_y = read_perfect_distort_xy(filename, verbose=True)
