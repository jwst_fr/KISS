#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 2 March 2018  KISS package
#  author R Gastaud

from astropy.io import fits
import matplotlib.pyplot as plt
#
from kiss.steps.read_catalog import read_catalog_fits

#
filename = '../kiss_0/JA_LMC_MIRI_v2.fits.gz'
hdulist = fits.open(filename)
kuse = hdulist[1].data['kuse']
#
filename = 'JA_LMC_MIRI_v2.fits.gz'
filter_name = 'F220X'
id, ra, dec, flux = read_catalog_fits(filename)

plt.figure(1)
plt.plot(ra, dec, '+')
plt.figure(2)
plt.plot(flux, 'x')
plt.plot(kuse, '+')
flux.min(), flux.max()
