#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
 This script belongs to the KISS package.
 It creates a catalog of 5 stars,
 one at the center of the detector, the 4 others at the corner   with spacing delta.
 It computes the ra, dec for each star for a given pointing p_ra, p_dec, pa.
 The output is the file catalog_5stars.dat

:History:
03 March 2018: Created.
09 March 2018: simplify use w.wcs_pix2world

@author:  R Gastaud (CEA Saclay)
"""

from __future__ import print_function
import numpy as np
from astropy import wcs
from astropy.table import Table, Column
import matplotlib.pyplot as plt

PLOTTING = True
filterName = 'F1000W'
p_ra = 80.5
p_dec = -69.5
###  do no rotate the stars and after we shall rotate the camera
#     GOOD IDEA
pa = 0  # better
out_file = 'catalog_5stars_0.dat'
#
delta = 30  # arcsecond
x = np.array([0, delta, -delta, -delta, delta])
y = np.array([0, delta, delta, -delta, -delta])
nn = len(x)
#
# flux = np.arange(nn) + 1.
#  put the brightest source at the center
flux = 5. - np.arange(nn)  # milliJansky, descending order
#
#
w = wcs.WCS(naxis=2)
w.wcs.crval = np.array([p_ra, p_dec])
w.wcs.ctype = ["RA---TAN", "DEC--TAN"]
# w.wcs.cdelt = np.array([-1., 1.]) / 3600.  No the x is ra, so no negative value
w.wcs.cdelt = np.array([1., 1.]) / 3600.
w.wcs.crota = [pa, pa]
# @crpix = np.array([688.5, 511.5])
crpix = np.array([0, 0])
w.wcs.crpix = crpix
#  do not use w.wcs_pix2world
ra, dec = w.wcs_pix2world(x, y, 1)
#
for i in np.arange(nn):
    print(i, ra[i], dec[i], flux[i])

if PLOTTING:
    plt.figure(1)
    plt.title('x,y')
    plt.plot(x, y, '+')
    plt.plot(x, y)
    plt.figure(2)
    plt.title('ra, dec')
    plt.plot(ra, dec, '+')
    plt.plot(ra, dec)

###############
# http://docs.astropy.org/en/stable/units/equivalencies.html


strArr = np.empty(nn, dtype='str')  # compatibility with python 3. ("string" is only python 2.7)
for i in range(0, nn):
    strArr[i] = str(i)

col1 = Column(strArr, name='id', dtype='str')
col2 = Column(ra, name='ra', unit='deg', dtype='float64')
col3 = Column(dec, name='dec', unit='deg', dtype='float64')
col4 = Column(flux, name='flux', unit='milliJansky', dtype='float64')
col5 = Column(np.repeat(10., nn), name='wavelength', unit='micron', dtype='float64')
t = Table([col1, col2, col3, col4, col5])
print("write file ", out_file)
t.write(out_file, format='ascii.ecsv')
