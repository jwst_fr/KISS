#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 3 March 2018  KISS package
#  author R Gastaud
#

from astropy.io import ascii
# http://docs.astropy.org/en/stable/units/equivalencies.html
from astropy import units as u
#
file_name = 'catalog_5stars.dat'
data = ascii.read(file_name)
#
star_id = data['id'].data
ra = data['ra'].data*data['ra'].unit
dec = data['dec'].data*data['dec'].unit
flux = data['flux'].data*data['flux'].unit
wavelength = data['wavelength'].data*data['wavelength'].unit
#
wavelength.to(u.meter)
