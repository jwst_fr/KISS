# Create a dummy sky background
#  RG 16 June 2020

from astropy.modeling import models, fitting
from astropy.modeling.functional_models import Gaussian2D
from astropy.modeling.functional_models import TrapezoidDisk2D
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from astropy.io import fits
from astropy import wcs

#  dithering.dat in pixel lower than 50
nx=1400
ny=1500

###  create wcs
ra = 80.5
dec = -69.5 +20*0.11/3600
pa = 0
w = wcs.WCS(naxis=2)
w.wcs.crval = np.array([ra, dec])
w.wcs.ctype = ["RA---TAN", "DEC--TAN"]
cdelt = np.array([-0.11, 0.11])/3600
w.wcs.crota = [pa, pa]
w.wcs.crpix = np.array([(nx-1)/2, (ny-1)/2])
w.wcs.cdelt = cdelt
header = w.to_header()
print(repr(header))

##  create dummy image
y, x = np.mgrid[:ny, :nx]
gg = Gaussian2D(x_mean=(nx-1)/2., y_mean=(ny-1)/2., x_stddev=400, y_stddev=50, theta=0, amplitude=100.)
image = gg(x,y)
plt.imshow(image, origin='lower')
tt = TrapezoidDisk2D(x_0=400, y_0=650, R_0=50, slope=1, amplitude=50)
image2 = tt(x,y)
plt.imshow(image+image2, origin='lower')
image = image+image2

####  write fits file
hdu = fits.PrimaryHDU(image, header=header)
hdu.header['BUNIT']='electron/second'
hdu.header['FILTER']='F1000W'
hdu.header['FILENAME']='SKY_BACKGROUND'
hdu.header['AUTHOR']='Rene Gastaud'
hdu.header['DETECTOR']= 'MIRIMAGE'
#hdu.writeto('sky_background_F1000W.fits')
hdu.writeto('KISS_ZODIACAL_F1000W_0.fits')
