
import matplotlib.pyplot as plt
from astropy import units as u

from kiss.kiss import place_stars
from kiss.steps.kiss_psf import *
from astropy.io import fits


def check_one_star(kp, star_x, star_y):
    star_flux = np.array([10]) * u.milliJansky
    nz, ny, nx = kp.psfs.shape
    zoom = kp.oversampling
    x_ref = star_x * zoom + nx
    y_ref = star_y * zoom + ny
    star_x = np.array([star_x])
    star_y = np.array([star_y])
    big_image = place_stars(star_x, star_y, star_flux, kp)
    (y, x) = np.unravel_index(big_image.argmax(), big_image.shape)
    # print ('star yx', star_y, star_x,  'yx',y, x, 'yx ref', y_ref, x_ref)
    print ('dx', x - x_ref, 'dy', y - y_ref)
    return


kp = KissPSF('dummy')

check_one_star(kp, 511, 511)

check_one_star(kp, 0, 511)

check_one_star(kp, 1, 1024)

check_one_star(kp, 0, 1024)

check_one_star(kp, -1, 1024)

big_image = place_stars(xx / 4., xx / 4., star_flux, kp)
ligne = big_image[xx, xx]
plt.plot(ligne)
hdu = fits.PrimaryHDU(big_image)
hdu.writeto('newtable2.fits')
