#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 26 February 2018  KISS package
# 27  February 2018 add skyflat
#  author R Gastaud
#  version 3.1 add some information in the header date, username, catalog, configuration
#  version 3.2 add transmission for mirisim compatibility
#  version 3.3 add pixel area for background and kiss_psf for local variation of psf
#  version 3.5.2 correct the dithering  9th of July 2020
#  version 3.5.3 add sky_background and telescope_background 18th of October 2020
#  version 3.5.4 userfriendly more robust for inputs 25th of October 2020
#                 read_dither

"""
CDP_DIR environment variable needs to be set

Limitations:
* Only FAST mode (used to compute total time from frame/integration/exposure)

"""

import os
import sys
import time
import datetime
import getpass
from collections import OrderedDict

import numpy as np
from astropy import wcs
from pkg_resources import parse_version
from scipy import ndimage as nd
import astropy
from astropy.io import fits

from .steps import get_background
from .steps.sky_background import skyBackground
from .steps import ComputeDistortion
from .steps.kiss_psf import KissPSF
from .steps import mirim_rebin2d
from .steps import read_catalog
from .steps import read_flat
from .steps import get_flux_prefactor

from . import utils
from . import version

import logging

LOG = logging.getLogger('kiss')

# set up logging to file
utils.init_log(log="KISS.log", stdout_loglevel="INFO", file_loglevel="DEBUG")

if parse_version(np.__version__) < parse_version('1.13'):
    LOG.error("Numpy version too old (>= 1.13)")
    sys.exit()

if parse_version(astropy.__version__) < parse_version('1.3'):
    LOG.error("AstroPy version too old (>= 1.3)")
    sys.exit()


def get_det_coord(ra, dec, p_ra, p_dec, crpix, cdelt, pa):
    """
    convert star (ra,dec) coordinates
    into (x,y) coordinates (detector coordinates)
    Distortion is NOT included
    See world2pix_centered in kiss.scripts.compute_distortion_polynom_coef_radec

    BEWARE  crpix and cdelt should be compatible with
    the distortion polynoms FITS file
    
    :param ra: right ascension of the points of the grid in the sky, unit decimal degree
    :type ra:  numpy array of floats, shape npoints
    
    :param dec: declination  of the points of the grid in the sky, unit decimal degree
    :type dec:  numpy array of floats, shape npoints
    
    
    :param p_ra: right ascension of the pointing, unit decimal degree
    :type p_ra:  scalar, float
    
    :param p_dec: declination  of the pointing , unit decimal degree
    :type p_dec:  scalar, float

    :param crpix: the coordinates of the reference pixel in the detector (unit pixel)
    e.g. np.array([688.5, 511.5]) or np.array([511.5, 511.5])
    :type crpix:  numpy array of two floats

    :param pa: position angle, angle of the second axis of the detector with North, unit decimal degree
    :type pa: float

    :param cdelt: size of the detector pixel projected in sky, unit arsecond, default value 0.11
    :type cdelt: numpy array of floats, shape=2

    :type pa: float

    :return: (perfect_x, perfect_y, distorted_x, distorted_y, w)
    column: x coordinate of the projection of the points of the grid on the camera, unit pixel
    row: y coordinate of the projection of the points of the grid on the camera,unit pixel
    w: object to transform the coordinates world 2 pixel and pixel 2 world
    :rtype: Union[nd.array(float), nd.array(float), astropy.wcs.WCS]
"""

    # create a wcs
    w = wcs.WCS(naxis=2)
    w.wcs.crval = np.array([p_ra, p_dec])
    w.wcs.ctype = ["RA---TAN", "DEC--TAN"]
    w.wcs.cdelt = cdelt / 3600.
    w.wcs.crpix = crpix
    w.wcs.crota = [-pa] * 2  # <=> [pa, pa]

    column, row = w.wcs_world2pix(ra, dec, 1)

    return column, row, w

def generate_lines(filename):
    """
    Read a text file, sikp the comment lines (which start with #),
    and replace comma, semi-column, tab  by blank
    since version 3.5.4, see https://likegeeks.com/numpy-loadtxt-tutorial/
    :param str filename: dithering file.
    
    :return: return text, list of strings
    """
    delimiters=[',', ';', '\tab']
    text = []
    with open(filename) as f:
        for line in f:
            if not(line.startswith("#")):
                line = line.strip() #removes newline character from end
                for d in delimiters:
                    line =line.replace(d, " ")
                text.append(line)
    return text

def read_dither(filename):
    """
    Read dithering file: it is a text file, comments lines start with #, 2 columns, dx, xy.
    dx and dy are in pixels.
    Since version 3.5.4 separators can be blank, or comma, orsemi-column,or tab, or a  mixture.
    BEWARE : french version of Excel save files format "csv" with a coma for decimal point and a semicolumn for delimiter.
    this can not be read !

    :param str filename: dithering file. If no dither 'none' is given by ConfigObj Validator
    

    :return: return (dx, dy), list of pixel shift for each dither position
    """
    if filename != "none":
        # ndmin=2 ensure that we have an array if only one dither is provided
        text = generate_lines(filename)
        (x_dither, y_dither) = np.loadtxt(text, usecols=(0, 1), dtype=float,
                                          unpack=True, ndmin=2)
    else:
        # No dithering
        (x_dither, y_dither) = (np.array([0.]), np.array([0.]))

    return x_dither, y_dither


def place_stars(star_x, star_y, star_flux, kp):
    """
    Given stars position and flux, will position each star
    on an oversampled detector. Some stars might be
    left out because out-of-field

    The precision is about 1e-6 pixel (Spline order=3)

    :param nd.array(float) star_x: column position of the stars w.r.t detector center
    :param nd.array(float) star_y: line position of the stars w.r.t detector center
    :param nd.array(float) star_flux: star flux with astropy.unity in it

    :param object kiss.steps.KissPSF , kp: contains several oversampled PSF arrays

    :return: oversampled image of the detector (need to be rebinned)
    """
    psf_size_z, psf_size_y, psf_size_x = kp.psfs.shape
    zoom = kp.oversampling
    psf_margin_x = psf_size_x / zoom / 2 - 1
    psf_margin_y = psf_size_y / zoom / 2 - 1

    nstars = star_x.size

    #  select the useful stars
    visible_stars = np.where((star_x > -psf_margin_x) & (star_x < 1024 + psf_margin_x) & (star_y > -psf_margin_y) & (
            star_y < 1024 + psf_margin_y))
    x_select = star_x[visible_stars]
    y_select = star_y[visible_stars]
    flux_select = star_flux[visible_stars]
    nselect = x_select.size

    LOG.debug("Selected stars: {} (out of {})".format(nselect, nstars))

    nny = 2 * psf_size_y + int(zoom) * 1024
    nnx = 2 * psf_size_x + int(zoom) * 1024

    big_image = np.zeros([nny, nnx])
    # Lower-left corner and upper_right corner
    # for a hypothetical PSF at the middle of big image
    llc_ref_y = nny // 2 - psf_size_y // 2
    llc_ref_x = nnx // 2 - psf_size_x // 2
    urc_ref_y = llc_ref_y + psf_size_y
    urc_ref_x = llc_ref_x + psf_size_x

    # Star positions in oversampled image are with respect to detector center.
    # ix,iy are integer part, while dx,dy are the decimal part. Since ndimage.shift
    # is slow, we shift the decimal part on a small image, then the integer part
    # on the big image with a faster method
    (ix, dx) = np.divmod(zoom * (x_select - 512), 1)
    (iy, dy) = np.divmod(zoom * (y_select - 512), 1)
    ix = ix.astype(int)
    iy = iy.astype(int)

    # We shift them using the reference PSF position
    # (change origin from the center to the lower left corner of a detector without margins)
    llc_star_x = ix + llc_ref_x
    llc_star_y = iy + llc_ref_y
    urc_star_y = iy + urc_ref_y
    urc_star_x = ix + urc_ref_x

    # Place one PSF for each star, with the correct flux
    for i in np.arange(nselect):
        my_psf = kp.get(x_select[i], y_select[i])

        # shift with interpolation for precise shifting of the decimal part
        # With mode=constant, no need for extra margins, oof psf will be shifted and the rest is
        # set to 0 for that PSF
        my_psf = nd.shift(my_psf, [dy[i], dx[i]], order=1, mode="constant", cval=0.)

        # Force negative values to 0 to avoid crash in the poisson noise part.
        my_psf[my_psf < 0] = 0

        # Renormalisation
        my_psf = my_psf * (flux_select[i].value / my_psf.sum())

        big_image[llc_star_y[i]:urc_star_y[i], llc_star_x[i]:urc_star_x[i]] += my_psf

    return big_image


def generate_no_noise_metadata(config, ini_filename, wavelength):
    """
    Generate metadata dictionary for the no noise image

    :param config: ini file configuration
    :type config: ConfigObj
    :param ini_filename: ini filename
    :type ini_filename: str
    :param wavelength: central wavelength
    :type wavelength: float

    :return: metadata dictionary
    :rtype: dict
    """

    conf_meta = config["metadata"]
    obsid = conf_meta["obsid"]
    programid = conf_meta["program"]


    # Due to the loop, the metadata dictionary must be rewritten each time
    # Else, we'll have keys from noise FITS in the illum_models
    metadata = OrderedDict()
    metadata["KISS"] = "KISS v{}".format(version.__version__)
    for key in ["gain", "psf", "pce", "skyflat", "pix_area", "transmission", "distortion"]:
        try:
            metakey = "hierarch {}".format(key) if len(key) > 8 else key
            metadata[metakey] = config["calibration"][key]
        except KeyError:
            pass

    metadata["FILTER"] = (config["processing"]["filtername"], "Name of the filter element used")
    metadata["OBSERVTN"] = f"{obsid:03d}"
    metadata["PROGRAM"] = f"{programid:05d}"
    metadata["INSTRUME"] = ("MIRI", "Instrument used to acquire the data")
    metadata["APERNAME"] = ("MIRIM_FULL", "PRD science aperture used")
    metadata["DETECTOR"] = ("MIRIMAGE", "Name of the detector used to acquire the data")
    metadata["EXP_TYPE"] = ("MIR_IMAGE", "Name of detector used to acquire the data")

    metadata["SUBARRAY"] = ("FULL", "Subarray used")
    metadata["SUBSTRT1"] = (1, "Starting pixel in axis 1 direction")
    metadata["SUBSTRT2"] = (1, "Starting pixel in axis 2 direction")
    metadata["SUBSIZE1"] = (1032, "Number of pixels in axis 1 direction")
    metadata["SUBSIZE2"] = (1024, "Number of pixels in axis 2 direction")
    metadata["FASTAXIS"] = (1, "Fast readout axis direction")
    metadata["SLOWAXIS"] = (2, "Slow readout axis direction")

    metadata["catalog"] = config["processing"]["catalog"]
    # astropy version 4.0.dev24994 does not support both HIERARCH and CONTINUE
    # https://github.com/astropy/astropy/issues/3746
    # metadata["sky_background_file"] = config["processing"]["sky_background_file"]
    # metadata["telescope_background_file"] = config["processing"]["telescope_background_file"]
    metadata["sky_bkg"] = config["processing"]["sky_background_file"]
    metadata["tel_bkg"] = config["processing"]["telescope_background_file"]

    metadata["config"] = ini_filename
    format = '%Y-%m-%d-%H-%M-%S'
    today = datetime.datetime.now()
    metadata['date'] = today.strftime(format)
    metadata['DATE-OBS'] = today.strftime("%Y-%m-%d")
    metadata['TIME-OBS'] = today.strftime("%H:%M:%S.%f")
    metadata['user'] = getpass.getuser()
    metadata["waveleng"] = (wavelength, 'micron')

    # Write Illum Model
    metadata["BUNIT"] = "electron/s"

    return metadata


def generate_noise_metadata(config, ini_filename, wavelength, total_time):
    """
    Generate metadata dictionary for the noisy image

    :param config: ini file configuration
    :type config: ConfigObj
    :param ini_filename: ini filename
    :type ini_filename: str
    :param wavelength: central wavelength
    :type wavelength: float
    :param total_time: total integration time
    :type total_time: float

    :return: metadata dictionary
    :rtype: dict
    """
    metadata = generate_no_noise_metadata(config, ini_filename, wavelength)

    # overwrite unit declared by the no noise function
    metadata["BUNIT"] = "DN/s"

    metadata["READPATT"] = ("FAST", "Readout pattern")
    metadata["NINTS"] = (config["noise"]["integrations"], "Number of integrations per exposure")
    metadata["NEXPS"] = (config["noise"]["exposures"], "")
    metadata["NFRAMES"] = (config["noise"]["frames"], "Number of frames per group")
    metadata["NGROUPS"] = (config["noise"]["frames"], "Number of groups per integration")
    metadata["TFRAME"] = (config["noise"]["tframe"], "[s] Time between groups")
    metadata["EFFEXPTM"] = (total_time, '[s] Effective exposure time')
    metadata["hierarch pixelflat"] = config["calibration"]["pixelflat"]

    return metadata


def kiss(ini_filename):
    """
        Simulate a MIRI Imager FULL array image

        By default without noise, but a second image
        with noise can be written.

        Input are a catalog of stars and dithering positions

        :param str ini_filename: INI file filename
        :param bool verbose: Debug info added

        :return:
        * For each dithering position, write a fits noiseless FITS file (in electron/s)
        * For each dithering position, write also a noisy FITS file (in DN/s) if noise was requested
        """

    config = utils.get_config(ini_filename)

    LOG.debug("KISS v{}".format(version.__version__))

    out_extension = "fits"  # Extension for image output

    if not os.path.exists(config["processing"]["dir"]):
        os.makedirs(config["processing"]["dir"])

    catalog_filename = config["processing"]["catalog"]
    filter_name = config["processing"]["filtername"]
    wavelength = float(filter_name[1:-1]) / 100.

    # initialise the distortion object = read the polynoms coefficients.
    distortion_object = ComputeDistortion(config["calibration"]["distortion"])
    crpix = np.array([distortion_object.crpix1, distortion_object.crpix2])
    cdelt = np.array([-0.11, 0.11])  # arcsecond should be read for distortion_object

    (ja_id, ra, dec, filter_flux, filter_wav) = read_catalog(catalog_filename, filter_name)

    (x_undist, y_undist, ref_wcs) = get_det_coord(ra, dec, config["pointing"]["ra"], config["pointing"]["dec"],
                                                  crpix, cdelt, config["pointing"]["pa"])

    # read PSF
    # mypsf, zoom = read_psf(config["calibration"]["psf"], 512, 512)
    # psf_size_x, psf_size_y = mypsf.shape
    kp = KissPSF(config["calibration"]["psf"])
    zoom = kp.oversampling
    psf_size_z, psf_size_y, psf_size_x = kp.psfs.shape

    skyflat = read_flat(config["calibration"]["skyflat"])
    if not isinstance(skyflat, int):
        skyflat = skyflat[:, 4:-4]

    pix_area = read_flat(config["calibration"]["pix_area"])
    pix_area = pix_area[:, 4:-4]

    no_noise_metadata = generate_no_noise_metadata(config, ini_filename, wavelength)

    noise_method = config["noise"]["method"].lower()
    if noise_method == "simple":
        pixelflat = read_flat(config["calibration"]["pixelflat"])
        if not isinstance(pixelflat, int):
            pixelflat = pixelflat[:, 4:-4]

        total_time = config["noise"]["tframe"] * \
                     config["noise"]["frames"] * config["noise"]["exposures"] * config["noise"]["integrations"]

        noise_metadata = generate_noise_metadata(config, ini_filename, wavelength, total_time)

    (x_dither, y_dither) = read_dither(config["pointing"]["dithering"])

    # flux prefactor computed for 'pref_wav'
    mjky2electron, pref_wav = get_flux_prefactor(config["calibration"]["pce"])
    # multiply by detoriation factor
    mjky2electron = mjky2electron * float(config["calibration"]["transmission"])
    # why float ???? it was a string but configspec.ini said integer

    # Flux of the stars at 'pref_wav' in Electron/s
    elec_flux = mjky2electron * filter_flux
    background_flux = get_background(filter_name)

    ## sky  background version 3.5.3
    sky_background_file = config["processing"]["sky_background_file"]
    if(sky_background_file != 'none'):
        sky_background = skyBackground(sky_background_file, distortion_object)

    ## telescope  background version 3.5.3
    telescope_background_file = config["processing"]["telescope_background_file"]
    if(telescope_background_file != 'none'):
        telescope_background = fits.getdata(telescope_background_file)

    # We force the seed value if needed
    seed = config["noise"]["seed"]
    if not config["noise"]["seed"]:
        seed = None
    np.random.seed(seed=seed)

    LOG.info("Noise method: {} (seed={})\n".format(noise_method, np.random.get_state()[1][0]))

    dither_id = 0
    nb_dither = len(x_dither)
    ref_crpix = (ref_wcs.wcs.crpix).copy()
    for (dither_dx, dither_dy) in zip(x_dither, y_dither):
        dither_id += 1
        # dither_wcs = ref_wcs.copy()  copy does not work, should use deep copy
        # Shift the WCS depending on the dither position
        ref_wcs.wcs.crpix = ref_crpix + (dither_dx, dither_dy)  # the 9th of July 2020

        print(
            "Compute image {}/{} with dither (dx,dy) = ({:.3f}, {:.3f})".format(dither_id, nb_dither, dither_dx,
                                                                                dither_dy))

        x_shifted = x_undist + dither_dx
        y_shifted = y_undist + dither_dy

        #  add distortion
        x_dist, y_dist = distortion_object.foc2pix(x_shifted, y_shifted)

        oversampled_image = place_stars(x_dist, y_dist, elec_flux, kp)

        # get rid of margins initially added for stars that partially illuminate the detector
        cropped_image = oversampled_image[psf_size_y:-psf_size_y, psf_size_x:-psf_size_x]

        final_image = mirim_rebin2d(cropped_image, [int(zoom), int(zoom)])

        final_image += background_flux * pix_area
        
        # sky  background version 3.5.3
        if(sky_background_file != 'none'):
            final_image += (sky_background.get(ref_wcs)* pix_area)

        final_image = final_image * skyflat

        # telescope  background version 3.5.3
        if(telescope_background_file != 'none'):
            final_image += telescope_background

        # prepare basename for the dither following JWST naming convention:
        conf_meta = config["metadata"]
        programid = conf_meta["program"]
        obsid = conf_meta["obsid"]
        out_prefix = f"jw{programid:05d}{obsid:03d}001_01101_{dither_id:05d}_mirimage"

        illum_filename = f'{out_prefix}_illum.{out_extension}'
        path = os.path.join(config["processing"]["dir"], illum_filename)
        utils.write_fits(final_image, filename=path, metadata=no_noise_metadata, wcs=ref_wcs)

        # Simple Noise
        if noise_method == "simple":
            # mask forced to zero instead of NaN
            np.nan_to_num(pixelflat, copy=False)

            final_image *= pixelflat  # Electron/s

            final_image *= total_time  # Electron

            final_image = np.random.poisson(final_image)

            final_image = final_image.astype(float) / (config["calibration"]["gain"] * total_time)  # DN/s

            # Overwrite metadata for that exposure
            noise_metadata["EXPOSURE"] = dither_id

            # Write Noisy image
            noise_filename = f'{out_prefix}_cal.{out_extension}'
            path = os.path.join(config["processing"]["dir"], noise_filename)
            utils.write_fits(final_image, filename=path, metadata=noise_metadata, wcs=ref_wcs)

        # SCASim
        elif noise_method == "scasim":
            # to be checked
            from .steps import call_scasim
            scasim_filename = "{}_scasim_{}.{}".format(config["processing"]["fileprefix"], dither_id, out_extension)
            out_file = os.path.join(config["processing"]["dir"], scasim_filename)
            call_scasim.call_scasim(final_image, image, config, out_file)

    return final_image


# The following code is for development and testing only
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm

    print("Testing  kiss routine")
    PLOTTING = True  # Set to False to turn off plotting.
    config = utils.get_config("configuration.ini")

    t0 = time.time()
    image = kiss(config)
    t1 = time.time()
    print('Elapsed time for kiss: {:.2f} s'.format(t1 - t0))

    if PLOTTING:
        plt.figure(1)
        plt.title("{} simulation".format(config["processing"]["filtername"]))
        plt.imshow(image, norm=LogNorm(), origin='lower', cmap='gist_stern')
        plt.colorbar(orientation='horizontal')
        plt.savefig('simulation.pdf')
        plt.show()

    LOG.info("\nTest finished.")
