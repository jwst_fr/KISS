#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Utilities not directly related to scientific algorithms
"""
from astropy.io import fits
import numpy as np
import pkg_resources
import configobj
import validate
import os
import sys

import logging
LOG = logging.getLogger('kiss.utils')


def get_nested(data, args):
    """
    Allow to get value in dictionnary tree

    Used in ConfigObj validator

    If ones want to get toto["section1"]["s2]["key"]
    call:
    value = get_nested(toto, ["section1", "s2", "key"])

    Parameter:
    :param dict data: input dict to get data on
    :param list(str) args: list of keys to use recursively

    :return: value corresponding to the list of keys
    """
    value = data.copy()
    for key in args:
        value = value.get(key)

    return value


def get_config(filename):
    """
    Read then validate and convert the input file
    BEWARE there must be a file named 'configspec.ini' in the directory of kiss
    
    :param str filename: .ini filename

    :return ConfigObj:  config file
    """

    if not os.path.isfile(filename):
        LOG.error("The file '{}' can't be found".format(filename))
        sys.exit()

    # Prepare to convert values in the config file
    val = validate.Validator()
    specfile = pkg_resources.resource_filename('kiss', 'configspec.ini')
    configspec = configobj.ConfigObj(specfile, list_values=False)

    config = configobj.ConfigObj(filename, configspec=configspec, raise_errors=True)

    # Check and convert values in config.ini (i.e str to float or integer/bool)
    results = config.validate(val, preserve_errors=True)

    for entry in configobj.flatten_errors(config, results):

        [section_list, key, error] = entry
        section_list.append(key)

        if not error:
            msg = "The parameter %s was not in the config file\n" % key
            msg += "Please check to make sure this parameter is present and there are no mis-spellings."
            raise ValueError(msg)

        if key is not None and isinstance(error, validate.VdtValueError):
            option_string = get_nested(configspec, section_list)
            msg = "The parameter {} was set to {} which is not one of the allowed values\n".format(
                key, get_nested(config, section_list))
            msg += "Please set the value to be in {}".format(option_string)
            raise ValueError(msg)

    # Deprecated
    if "fileprefix" in config["processing"]:
        LOG.warning(f"Parameter [processing][fileprefix] is not used anymore, you can safely "
                    f"delete it from your config file.")

    return config


def init_log(log="kiss.log", stdout_loglevel="INFO", file_loglevel="DEBUG"):
    """

    :param str log: filename where to store logs. By default "kiss.log"
    :param str stdout_loglevel: log level for standard output (ERROR, WARNING, INFO, DEBUG)
    :param str file_loglevel: log level for log file (ERROR, WARNING, INFO, DEBUG)
    :return:
    :rtype:
    """

    import logging.config

    log_config = {
        "version": 1,
        "formatters":
            {
                "form01":
                    {
                        "format": "%(asctime)s %(levelname)-8s %(message)s",
                        "datefmt": "%H:%M:%S"
                    },
                "form02":
                    {
                        "format": "%(asctime)s [%(name)s] %(levelname)s - %(message)s",
                        "datefmt": "%H:%M:%S"
                    },
            },
        "handlers":
            {
                "console":
                    {
                        "class": "logging.StreamHandler",
                        "formatter": "form01",
                        "level": stdout_loglevel,
                        "stream": "ext://sys.stdout",
                    },
                "file":
                    {
                        "class": "logging.FileHandler",
                        "formatter": "form02",
                        "level": file_loglevel,
                        "filename": log,
                        "mode": "w", # Overwrite file if it exists
                    },
            },
        "loggers":
            {
                "":
                    {
                        "level": "NOTSET",
                        "handlers": ["console", "file"],
                    },
            },
        "disable_existing_loggers": False,
    }

    logging.config.dictConfig(log_config)


def write_fits(image, filename, metadata, wcs):
    """
    Write image into FITS file. Add metadata

    :param nd.array image: numpy 2D array

    :param str filename: filename for the FITS file

    :param dict metadata: dictionnary containing all the metadata for the FITS file

    :param astropy.wcs wcs: WCS informations for our FITS file

    :return: Return nothing. Write to a FITS file
    """

    # Write to FITS
    header = wcs.to_header()

    # Add extra keywords
    header["FILENAME"] = (os.path.basename(filename), "Name of the file")

    for (key, value) in metadata.items():
        if len(key) > 8 and not key.startswith("hierarch"):
            key = f"hierarch {key}"

        if type(value) is tuple:
            if len(value) > 2:
                raise ValueError("Tuple must have 2 elements (value, comment)")
            item = (key, value[0], value[1])
        else:
            item = (key, value)

        header.append(item, end=True)

    phdu = fits.PrimaryHDU(header=header)
    
    sci_hdu = fits.ImageHDU(image, name="SCI")
    err_hdu = fits.ImageHDU(name="SCI")

    fake_mask = np.zeros_like(image, dtype="uint")
    dq_hdu = fits.ImageHDU(fake_mask, name="DQ")
    
    hdulist = fits.HDUList([phdu, sci_hdu, err_hdu, dq_hdu])

    hdulist.writeto(filename, overwrite=True)
    hdulist.close()
