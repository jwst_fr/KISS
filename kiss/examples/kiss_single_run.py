#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
To test the conditions on one simulation before the big jump
"""

from kiss.kiss import kiss
import time

#simulation = "simulation_test.ini"
simulation = "configuration.ini"


t0 = time.time()
image = kiss(simulation)
t1 = time.time()
print(f"Simulation done in {t1-t0} s")

