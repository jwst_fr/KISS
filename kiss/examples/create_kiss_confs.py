"""
Use predefined lists to create a list of KISS ini files in one go
"""

import glob
import copy
import os
import numpy as np
import configobj


def get_latest_cdp(cdp_dir, keys):
    """
    Given the location of CDP files

    :param str cdp_dir: Absolute or relative path
    :param keys: list of keys to find the CDP files (e.g ["_MIRIMAGE_", "F1000W_", "PSF_"])
    :type keys: list(str)
    :return: Name of lastest CDP files corresponding to the query
    """
    pattern = "*".join(keys)
    pattern = f"*{pattern}*.fits"
    cdps = glob.glob(os.path.join(cdp_dir, pattern))

    version_hash = []  # the biggest the hash, the latest the file
    for cdp in cdps:
        v = cdp.split("_")[-1].rstrip(".fits")
        t1, t2, t3 = v.split(".")
        # Sometimes t1 has a letter, we need to check that
        try:
            n1 = int(t1)
            n1b = 0
        except ValueError:
            n1 = int(t1[0])
            n1b = ord(t1[1]) - ord("A") + 1  # gives a number corresponding to a letter, A being 1
        n2 = int(t2)
        n3 = int(t3)
        tmp_hash = n3 + n2 * 10 + n1b * 100 + n1 * 1000
        version_hash.append(tmp_hash)

    latest_idx = np.argmax(version_hash)

    return cdps[latest_idx]


CDP_DIR = "/local/home/ccossou/miricle/CDP"

simulations = [
    {"filter": 'F770W', "ngroup": 10, "nint": 80, "dither": "dither_2point.dat"},
    {"filter": 'F1000W', "ngroup": 10, "nint": 80, "dither": "dither_2point.dat"},
    {"filter": 'F1500W', "ngroup": 10, "nint": 80, "dither": "dither_2point.dat"},
    {"filter": 'F2550W', "ngroup": 10, "nint": 80, "dither": "dither_2point.dat"},
    {"filter": 'F770W', "ngroup": 10, "nint": 20, "dither": "dither_cycling_large.dat"},
    {"filter": 'F1000W', "ngroup": 10, "nint": 20, "dither": "dither_cycling_large.dat"},
    {"filter": 'F1500W', "ngroup": 10, "nint": 20, "dither": "dither_cycling_large.dat"},
    {"filter": 'F2550W', "ngroup": 10, "nint": 20, "dither": "dither_cycling_large.dat"},
    {"filter": 'F770W', "ngroup": 10, "nint": 13, "dither": "dither_reuleaux.dat"},
    {"filter": 'F1000W', "ngroup": 10, "nint": 13, "dither": "dither_reuleaux.dat"},
    {"filter": 'F1500W', "ngroup": 10, "nint": 13, "dither": "dither_reuleaux.dat"},
    {"filter": 'F2550W', "ngroup": 10, "nint": 13, "dither": "dither_reuleaux.dat"},

    {"filter": 'F560W', "ngroup": 10, "nint": 40, "dither": "dither_2point.dat"},
    {"filter": 'F1130W', "ngroup": 10, "nint": 40, "dither": "dither_2point.dat"},
    {"filter": 'F1280W', "ngroup": 10, "nint": 40, "dither": "dither_2point.dat"},
    {"filter": 'F1800W', "ngroup": 10, "nint": 40, "dither": "dither_2point.dat"},
    {"filter": 'F2100W', "ngroup": 10, "nint": 40, "dither": "dither_2point.dat"},
    {"filter": 'F560W', "ngroup": 10, "nint": 10, "dither": "dither_cycling_large.dat"},
    {"filter": 'F1130W', "ngroup": 10, "nint": 10, "dither": "dither_cycling_large.dat"},
    {"filter": 'F1280W', "ngroup": 10, "nint": 10, "dither": "dither_cycling_large.dat"},
    {"filter": 'F1800W', "ngroup": 10, "nint": 10, "dither": "dither_cycling_large.dat"},
    {"filter": 'F2100W', "ngroup": 10, "nint": 10, "dither": "dither_cycling_large.dat"},
    {"filter": 'F560W', "ngroup": 10, "nint": 7, "dither": "dither_reuleaux.dat"},
    {"filter": 'F1130W', "ngroup": 10, "nint": 7, "dither": "dither_reuleaux.dat"},
    {"filter": 'F1280W', "ngroup": 10, "nint": 7, "dither": "dither_reuleaux.dat"},
    {"filter": 'F1800W', "ngroup": 10, "nint": 7, "dither": "dither_reuleaux.dat"},
    {"filter": 'F2100W', "ngroup": 10, "nint": 7, "dither": "dither_reuleaux.dat"},

    {"filter": 'F560W', "ngroup": 10, "nint": 20, "dither": "dither_4point_short.dat"},
    {"filter": 'F1130W', "ngroup": 10, "nint": 20, "dither": "dither_4point_short.dat"},
    {"filter": 'F770W', "ngroup": 10, "nint": 40, "dither": "dither_4point_short.dat"},
    {"filter": 'F1000W', "ngroup": 10, "nint": 40, "dither": "dither_4point_short.dat"},

    {"filter": 'F1500W', "ngroup": 10, "nint": 40, "dither": "dither_4point_long.dat"},
    {"filter": 'F2550W', "ngroup": 10, "nint": 40, "dither": "dither_4point_long.dat"},
    {"filter": 'F1280W', "ngroup": 10, "nint": 20, "dither": "dither_4point_long.dat"},
    {"filter": 'F1800W', "ngroup": 10, "nint": 20, "dither": "dither_4point_long.dat"},
    {"filter": 'F2100W', "ngroup": 10, "nint": 20, "dither": "dither_4point_long.dat"},
]

common_config = {'pointing': {'ra': '80.507682',
                              'dec': '-69.529550',
                              'pa': '-4.45', },
                 'calibration': {'distortion': 'distortion_polynoms_mirisim.fits',
                                 'gain': '5.5',
                                 'pix_area': 'MIRI_FM_MIRIMAGE_AREA_07.00.00.fits',
                                 'transmission': '0.9'},
                 'processing': {'catalog': 'JA_LMC_MIRI_v2.fits.gz'},
                 'noise': {'exposures': '1',
                           'tframe': '2.775',
                           'method': 'simple'}}

simulation_id = 1
for sim in simulations:
    config = copy.deepcopy(common_config)

    filter = sim["filter"]
    dither = sim["dither"][7:-4]  # Get rid of extension and prefix

    dir_name = f"{filter}_{dither}"

    config["pointing"]["dithering"] = sim["dither"]
    config["processing"]["filtername"] = sim["filter"]
    config["processing"]["dir"] = dir_name
    config["noise"]["integrations"] = sim["nint"]
    config["noise"]["frames"] = sim["ngroup"]

    # CDP
    config["calibration"]["psf"] = os.path.basename(get_latest_cdp(CDP_DIR, ["_MIRIMAGE_", f"{filter}_", "PSF_"]))
    config["calibration"]["pce"] = os.path.basename(get_latest_cdp(CDP_DIR, ["_MIRIMAGE_", f"{filter}_", "PCE_"]))
    config["calibration"]["skyflat"] = os.path.basename(
        get_latest_cdp(CDP_DIR, ["_MIRIMAGE_", f"{filter}_", "SKYFLAT_"]))
    config["calibration"]["pixelflat"] = os.path.basename(
        get_latest_cdp(CDP_DIR, ["_MIRIMAGE_FAST_", f"{filter}_PIXELFLAT_"]))

    conf_obj = configobj.ConfigObj(config)
    conf_obj.filename = f"simulation_{simulation_id}_{dir_name}.ini"
    conf_obj.write()

    simulation_id += 1
