#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Using pipeline_parallel for the parallel part, will run multiple KISS simulations

The simulations have to be generated (i.e a folder for each simulation, with inside all the
necessary files (simulation.ini)
"""

import pipeline_parallel
pipeline_parallel.force_single_thread()
import glob

from kiss.kiss import kiss


def run_single_kiss(config_file):
    """
    Given a folder created by mirisim.apt.run (with dryrun=True), will run the corresponding MIRISim simulation

    :param str folder: folder containing scene.ini, simulation.ini and simulator.ini file

    :return:
    """

    image = kiss(config_file)


# Now run the parallel part on the simulations created
extra_config = {"loggers":
    {

        "paramiko":
            {
                "level": "WARNING",
            },

        "matplotlib":
            {
                "level": "WARNING",
            },

        "astropy":
            {
                "level": "WARNING",
            },
    }, }

pipeline_parallel.init_log(log="KISS.log", stdout_loglevel="INFO", file_loglevel="DEBUG", extra_config=extra_config)


# Create all simulations folders
simulations = glob.glob("simulation_*.ini")

ram = [5.] * len(simulations)  # For now, 10GB per simulation, because I don't know how much it needs yet

folders = pipeline_parallel.ArgList(args=simulations, ram=ram)

pool = pipeline_parallel.ProcessPool(func=run_single_kiss, params=folders, delay=3)  # 2s delay forced to avoid disk problems
pool.run()
