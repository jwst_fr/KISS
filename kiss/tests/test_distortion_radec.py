from __future__ import print_function

"""
Created on 08 March 2018
@author: Rene Gastaud

history : 

"""
import pkg_resources
import os
import numpy as np
import numpy.testing as npt

from kiss.steps import ComputeDistortion
from kiss.steps.compute_distortion import __version__ as cd_version

from kiss.scripts.compute_distortion_polynom_coef_radec import read_distortion_reference_point, world2pix_centered

__version__ = 1


def test_distortion_radec(grid_file='MIRIM_grid_radec.fits', verbose=False):
    """

    RA, DEC: right ascension and declination in arcsecond, as in the official MIRIM simulator
                    RA=0 and DEC=0 at the reference point of the simulator (688.5 511.5)
    COL, ROW : Detector Coordinates, from 0 to 1023

    :param grid_file: File containing 2 set of coordinates (RA,DEC) and (COL, ROW)
    :type grid_file: str
    :param verbose:
    :type verbose: bool
    """

    ######### hard coded #########################
    pa = -4.45  # decimal degree
    crpix = np.array([688.5, 511.5])
    cdelt = np.array([-0.11, 0.11])
    poly_file = 'distortion_polynoms_mirisim.fits'
    ######### end of hard coded ###################

    ref_file = pkg_resources.resource_filename('kiss', os.path.join("tests", grid_file))

    ra, dec, col, row = read_distortion_reference_point(ref_file)

    perfect_x, perfect_y, distorted_x, distorted_y, w = world2pix_centered(ra, dec, col, row, crpix, cdelt, pa)

    perfect_x += crpix[0]
    perfect_y += crpix[1]
    ##############
    # perfect_x perfect_y in pixel from 0 to 1023
    #  col, row in pixel from 0 to 1023

    diff_x = col - perfect_x
    if verbose:
        print("COL diff(distord, undistord): min = {} ; max = {}".format(diff_x.min(), diff_x.max()))

    diff_y = row - perfect_y
    if verbose:
        print("ROW diff(distord, undistord): min = {} ; max = {}".format(diff_y.min(), diff_y.max()))

    ######### Now test compute distortion  ##############
    cd = ComputeDistortion(poly_file)
    atol = 0.0082  # in pixel
    cc, rr = cd.foc2pix(perfect_x, perfect_y)
    diff_x = cc - col
    if verbose:
        print("COL diff(distorted, computed): min = {} ; max = {}".format(diff_x.min(), diff_x.max()))
    npt.assert_allclose(col, cc, atol=atol)
    #
    diff_y = rr - row
    if verbose:
        print("ROW diff(distorted, computed): min = {} ; max = {}".format(diff_y.min(), diff_y.max()))
    npt.assert_allclose(row, rr, atol=atol)
    #
    rra, ddec = cd.pix2foc(col, row)
    #
    diff_x = rra - perfect_x
    if verbose:
        print("COL diff(undistorted, computed): min = {} ; max = {}".format(diff_x.min(), diff_x.max()))
    npt.assert_allclose(rra, perfect_x, atol=atol)
    #
    diff_y = ddec - perfect_y
    if verbose:
        print("ROW diff(undistorted, computed): min = {} ; max = {}".format(diff_y.min(), diff_y.max()))
    npt.assert_allclose(ddec, perfect_y, atol=atol)

    return


if __name__ == '__main__':
    print("test_distortion_official v{} computeDistortion v{}  ".format(__version__, cd_version))
    grid_file = 'MIRIM_grid_radec.fits'
    test_distortion_radec(grid_file, verbose=True)
