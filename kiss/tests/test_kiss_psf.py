#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
Created on 15 March 2018
@author: Rene Gastaud

history :

"""
import numpy.testing as npt

from kiss.steps.kiss_psf import *


def test_kiss_psf():
    kp = KissPSF('dummy')
    im1 = kp.get(333, 999)
    im2 = kp.get(333, -999)
    npt.assert_array_equal(im1, im2)
    assert (im1.sum() == 1)
    assert (im1.max() == 1)
    assert (im1.min() == 0)
    (y_1, x_1) = np.unravel_index(im1.argmax(), im1.shape)
    assert (x_1 == 255)
    assert (y_1 == 255)
    #
    filename = 'MIRI_FM_MIRIMAGE_F1000W_PSF_05.02.00.fits'
    kp = KissPSF(filename)
    # TO DO check that all psfs have maximum at 255,255
    for i in np.arange(9):
        print(i, np.unravel_index(kp.psfs[i, :, ].argmax(), kp.psfs[i, :, ].shape))
    #
    return
