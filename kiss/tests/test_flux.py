#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 26 February 2018  KISS package
#  author R Gastaud
"""
    This function test the flux computation
    """

import numpy as np

from kiss.steps import get_flux_prefactor


def test_flux(filename='MIRI_FM_MIRIMAGE_F1000W_PCE_06.00.00.fits', rtol=0.05, verbose=True):
    """
    Test the flux according to PASP IX page 692
    
    :param str filename: CDP filename for the PCE FITS
    :param float rtol: Relative tolerance (0.01 == 1%)
    :param bool verbose: Display extra information

    """
    ref_flux = 26800.  # electron/second from PASP IX page 692
    flux_hand = get_flux_prefactor(filename)
    flux_hand = flux_hand[0].value
    rdiff = (flux_hand - ref_flux) / ref_flux
    if verbose:
        print(
            "Filter: {} ; Flux(mirisim) = {:g} e-/s ; Flux(hand) = {:g} e-/s ; rdiff = {:.2f}%".format(filename,
                                                                                                       ref_flux,
                                                                                                       flux_hand,
                                                                                                       rdiff * 100))

        print("**************************************************")

    assert (np.fabs(rdiff) < rtol), " flux of the brightest pixel"
    return


#
# The following code is for development and testing only
#
if __name__ == '__main__':
    print("Testing the get_flux_prefactor routine")
    test_flux(rtol=1e-3)
