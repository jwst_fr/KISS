#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
Created on 15 March 2018
@author: Rene Gastaud

history :

"""
import os
import pkg_resources
import numpy as np
import numpy.testing as npt
#
import gzip
import shutil
#
from kiss.steps.read_write_catalog import *

def compress(filename):
    with open(filename, 'rb') as f_in, gzip.open(filename+'.gz', 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)
    return

def check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux ):
    npt.assert_array_equal(star_id, star_id_ref)
    npt.assert_array_equal(ra, ra_ref)
    npt.assert_array_equal(dec, dec_ref)
    npt.assert_array_equal(flux, flux_ref)
    return

def test_read_write_catalog():
    
    ########################################################################
    ###  check extrapolate flux
    flux = extrapolate_flux(20*u.mJy,  10*u.micron, 'F1000W')
    npt.assert_array_equal(flux, 20*u.mJy)
    
    flux = extrapolate_flux(20*u.mJy,  5*u.micron, 'F1000W')
    npt.assert_array_equal(flux, 5*u.mJy)
    
    ########################################################################
    ## create references from a subset of the LMC catalog
    filter_name = 'F1000W'
    filename = pkg_resources.resource_filename('kiss', os.path.join("../", 'JA_LMC_MIRI_v2.fits.gz'))
    star_id_0, ra_0, dec_0, flux_0 = read_catalog_fits(filename, filter_name)
    star_id_ref, ra_ref, dec_ref, flux_ref = random_select_catalog(10, star_id_0, ra_0, dec_0, flux_0)
    #wavelength_ref = (10. + np.arange(star_id_ref.size))*u.micron
    wavelength_ref = 5*np.ones(star_id_ref.size)*u.micron
    print(star_id_ref, ra_ref, dec_ref, flux_ref, wavelength_ref)
    
    ########################################################################
    ### check the format "simple" fits  (star_id, ra, dec, flux, wavelength)
    ## extrapolation with Rayleight Jeans approximation (10/5)**2 = 4
    filename1 = "dummy_simple.fits"
    write_catalog_fits_simple(filename1, star_id_ref, ra_ref, dec_ref, flux_ref, wavelength_ref)
    star_id, ra, dec, flux = read_catalog_fits(filename1, filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux*4)
    
    star_id, ra, dec, flux, filter_wave = read_catalog(filename1, filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux*4)
    
    compress(filename1)
    star_id, ra, dec, flux, filter_wave = read_catalog(filename1+'.gz', filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux*4)
   
   ########################################################################
    ### check the format "filter" fits  (star_id, ra, dec, flux_filter)
    filename2 = "dummy_filter.fits"
    write_catalog_fits_filter(filename2, filter_name, star_id_ref, ra_ref, dec_ref, flux_ref)
    star_id, ra, dec, flux = read_catalog_fits(filename2, filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux)
    
    star_id, ra, dec, flux, filter_wave = read_catalog(filename2, filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux)
  
    compress(filename2)
    star_id, ra, dec, flux, filter_wave = read_catalog(filename2+'.gz', filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux)

    ########################################################################
    ### check the format "filter" data  (star_id, ra, dec, flux_filter)
    filename3 = "dummy_filter.dat"
    write_catalog_dat_filter(filename3, filter_name, star_id_ref, ra_ref, dec_ref, flux_ref)
    star_id, ra, dec, flux = read_catalog_dat(filename3, filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux)
    
    star_id, ra, dec, flux, filter_wave = read_catalog(filename3, filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux)
  
    
    ########################################################################
    ### check the format "simple" data (star_id, ra, dec, flux, wavelength)
    ## Rayleight Jeans approximation (10/5)**2 = 4
    filename4 = "dummy_simple.dat"
    write_catalog_dat_simple(filename4, star_id_ref, ra_ref, dec_ref, flux_ref, wavelength_ref)
    star_id, ra, dec, flux = read_catalog_dat(filename4, filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux*4)
    
    star_id, ra, dec, flux, filter_wave = read_catalog(filename4, filter_name, verbose=True)
    check(star_id_ref, ra_ref, dec_ref, flux_ref, star_id, ra, dec, flux*4)

    ########################################################################
    ###
    filenames = [filename1, filename2, filename3, filename4, filename1+'.gz', filename2+'.gz']
    for filename in filenames:
        if os.path.exists(filename):
            os.remove(filename)
        else:
            print(filename +" does not exist")
    return
