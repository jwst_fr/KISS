from __future__ import print_function

"""
Created on 08 March 2018
@author: Rene Gastaud

history : 

"""

from astropy.io import fits
import numpy as np
from kiss.steps import ComputeDistortion
import pkg_resources
import os
__version__ = 0


def read_perfect_distort_xy(filename, verbose=False):
    """

    EFP: Entrance Focal Plane
    DFP: Detector Focal Plane

    :param filename: File containing EFP and DFP reference coordinates
    :type filename: str
    :param verbose:
    :type verbose: bool
    :return: x,y for undistorded and distorded coordinates
    :rtype: Union[nd.array, nd.array, nd.array, nd.array]
    """
    # 'MIRIM_grid_imaging.fits'
    hdulist1 = fits.open(filename)
    efp_x = hdulist1[1].data['EFP_X'] / 1000.
    efp_y = hdulist1[1].data['EFP_Y'] / 1000.
    dfp_x = hdulist1[1].data['DFP_X'] / 1000.
    dfp_y = hdulist1[1].data['DFP_Y'] / 1000.
    hdulist1.close()
    gx = 0.35163359841036418
    gy = 0.3543513074369713

    efp_x = efp_x.squeeze()
    efp_y = efp_y.squeeze()
    dfp_y = dfp_y.squeeze()
    dfp_x = dfp_x.squeeze()

    distorted_x = dfp_x * 40 + 511.5
    distorted_y = dfp_y * 40 + 511.5
    perfect_x = efp_x * 40 * gx + 511.5
    perfect_y = efp_y * 40 * gy + 511.5

    diff_x = distorted_x - perfect_x
    if verbose:
        print("X diff(distord, undistord): min = {} ; max = {}".format(diff_x.min(), diff_x.max()))

    diff_y = distorted_y - perfect_y
    if verbose:
        print("Y diff(distord, undistord): min = {} ; max = {}".format(diff_y.min(), diff_y.max()))

    return perfect_x, perfect_y, distorted_x, distorted_y


def array_is_close(array1, array2, atol):
    """

    :param array1: first array
    :type array1: nd.array
    :param array2: second array to compare to array1
    :type array2: nd.array
    :param atol: absolute tolerance
    :type atol: float

    :return: indexes where the arrays don't match. True = problem
    :rtype: nd.array
    """
    test_validated = np.isclose(array1, array2, atol=atol)

    (problems,) = np.where(test_validated == False)

    return problems


def display_errors(funcname, atol, x, y, fx, fy):
    """

    :param funcname: transformation function name
    :type funcname: str

    :param atol: absolute tolerance
    :type atol: float

    :param x: input X for which the transformation doesn't work well enough
    :type x: nd.array
    :param y: input Y for which the transformation doesn't work well enough
    :type y: nd.array

    :param fx: expected value to reach by the transformation
    :type fx: nd.array
    :param fy: expected value to reach by the transformation
    :type fy: nd.array

    :return:
    :rtype: str
    """
    msg = "\n"
    for (xi, yi, fxi, fyi) in zip(x, y, fx, fy):
        msg += "{}(({:.1f}, {:.1f})) - ({:.1f}, {:.1f}) > {:.1f}\n".format(funcname, xi, yi, fxi, fyi, atol)

    return msg


def test_distortion(verbose=False):
    """
    Test ComputeDistortion class and compare with reference points

    Distortion reference position come from Samuel Ronayette tests
    (report MIRIM FM Optical Tets Results, MIRI-RP-00919-CEA Issue 4.5)

    :param verbose:
    :type verbose: bool

    :return:
    :rtype:
    """

    tolerance = 0.4  # in pixel

    ref_file = pkg_resources.resource_filename('kiss', os.path.join("tests", 'MIRIM_grid_imaging.fits'))
    poly_file = 'distortion_polynoms.fits'

    cd = ComputeDistortion(poly_file)
    print("cd=", cd)
    perfect_x, perfect_y, distorted_x, distorted_y = read_perfect_distort_xy(ref_file, verbose=verbose)

    # Try to de-distord distorded position, and compare with the undistorded reference
    corrected_x, corrected_y = cd.pix2foc(distorted_x, distorted_y)

    x_problems = array_is_close(perfect_x, corrected_x, atol=tolerance)
    y_problems = array_is_close(perfect_y, corrected_y, atol=tolerance)

    if not verbose:
        assert (len(x_problems) == 0), "Correcting X distortion don't work for :" \
                                       "{}".format(display_errors("pix2foc", tolerance,
                                                                  distorted_x[x_problems], distorted_y[x_problems],
                                                                  perfect_x[x_problems], perfect_y[x_problems]))
        assert (len(y_problems) == 0), "Correcting Y distortion don't work for :" \
                                       "{}".format(display_errors("pix2foc", tolerance,
                                                                  distorted_x[y_problems], distorted_y[y_problems],
                                                                  perfect_x[y_problems], perfect_y[y_problems]))
    else:
        correct_dist_problems = np.union1d(x_problems, y_problems)

        print("pix2foc: {} points are above atol={}".format(correct_dist_problems.size, tolerance))

        import matplotlib.pyplot as plt
        fig = plt.figure()
        plot1 = fig.add_subplot(1, 2, 1)
        plot1.plot(distorted_x[correct_dist_problems], distorted_y[correct_dist_problems], 'bo')
        plot1.set_title("Problems when correcting distortion", size=10)
        plot1.set_xlabel("Distorded X [pixels]")
        plot1.set_ylabel("Distorded Y [pixels]")
        plot1.set_aspect('equal')
        plot1.xaxis.grid(True, which='minor', color='#000000', linestyle=':')
        plot1.yaxis.grid(True, which='minor', color='#000000', linestyle=':')
        plot1.xaxis.grid(True, which='major', color='#000000', linestyle='--')
        plot1.yaxis.grid(True, which='major', color='#000000', linestyle='--')

    ddistort_x, ddistort_y = cd.foc2pix(perfect_x, perfect_y)

    x_problems = array_is_close(distorted_x, ddistort_x, atol=tolerance)
    y_problems = array_is_close(distorted_y, ddistort_y, atol=tolerance)
    if not verbose:
        assert (len(x_problems) == 0), "Correcting X distortion don't work for :" \
                                       "{}".format(display_errors("foc2pix", tolerance,
                                                                  perfect_x[x_problems], perfect_y[x_problems],
                                                                  distorted_x[x_problems], distorted_y[x_problems]))
        assert (len(y_problems) == 0), "Correcting Y distortion don't work for :" \
                                       "{}".format(display_errors("foc2pix", tolerance,
                                                                  perfect_x[y_problems], perfect_y[y_problems],
                                                                  distorted_x[y_problems], distorted_y[y_problems]))
    else:
        apply_dist_problems = np.union1d(x_problems, y_problems)

        print("foc2pix: {} points are above atol={}".format(apply_dist_problems.size, tolerance))

        plot2 = fig.add_subplot(1, 2, 2)
        plot2.plot(perfect_x[apply_dist_problems], perfect_y[apply_dist_problems], 'ro')
        plot2.set_title("Problems when applying distortion", size=10)
        plot2.set_xlabel("Undistorded X [pixels]")
        plot2.set_ylabel("Undistorded Y [pixels]")
        plot2.set_aspect('equal')
        plot2.xaxis.grid(True, which='minor', color='#000000', linestyle=':')
        plot2.yaxis.grid(True, which='minor', color='#000000', linestyle=':')
        plot2.xaxis.grid(True, which='major', color='#000000', linestyle='--')
        plot2.yaxis.grid(True, which='major', color='#000000', linestyle='--')
        fig.subplots_adjust(left=0.12, bottom=0.1, right=0.96, top=0.96, wspace=0.26, hspace=0.26)
        fig.savefig("distortion.pdf")


if __name__ == '__main__':
    print("check compute_distortion v{}".format(__version__))
    test_distortion(verbose=True)
