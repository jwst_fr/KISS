# IPython log file
from __future__ import print_function

"""
Created on 15 March 2018
@author: Rene Gastaud

history :

"""
import numpy.testing as npt
from astropy import units as u

from kiss.kiss import place_stars
from kiss.steps.kiss_psf import *


def test_place_stars():
    kp = KissPSF('dummy')
    nz, ny, nx = kp.psfs.shape
    zoom = kp.oversampling
    star_x = np.array([511])
    star_y = np.array([511])
    x_ref = star_x * zoom + nx - 1
    y_ref = star_y * zoom + ny - 1  # problem odd/even half
    #
    star_flux = np.array([10]) * u.milliJansky
    print(star_x, star_y, star_flux)
    #
    big_image = place_stars(star_x, star_y, star_flux, kp)
    #
    big_ny, big_nx = big_image.shape
    assert ((zoom * 1024 + 2 * nx) == big_nx)
    npt.assert_almost_equal(big_image.sum(), star_flux.value, decimal=12)
    npt.assert_almost_equal(big_image.max(), star_flux.value, decimal=12)
    npt.assert_almost_equal(big_image.min(), 0.0000000000000, decimal=12)  # 12 significative digits will not mark 4e-16 equal to 0.0
    (y, x) = np.unravel_index(big_image.argmax(), big_image.shape)
    npt.assert_array_equal(x_ref, x)
    npt.assert_array_equal(y_ref, y)




# big_image.shape, big_image.min(), big_image.max(), big_image.sum()
# Out[22]: ((5120, 5120), 0.0, 10.0, 10.0)
# (y, x) (2555, 2555)
#  y_ref, x_ref (array([2556]), array([2556]))
