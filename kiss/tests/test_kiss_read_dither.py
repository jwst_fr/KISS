#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
Created on 15 March 2018
@author: Rene Gastaud

history :

"""
import os
import pkg_resources
import numpy as np
import numpy.testing as npt
from astropy import units as u
import numpy.testing as npt
#
from kiss.kiss import read_dither
#
def test_kiss_read_dither():
    ## reference : hard-coded
    x_ref = np.array([  0.   , -26.12 , -23.425, -21.83 , -16.495, -16.66 ])
    y_ref = np.array([  0.   , -41.   , -36.49 , -27.855, -30.055, -43.31 ])
    
    # test file with blank
    filename = pkg_resources.resource_filename('kiss', os.path.join("tests/", 'dithering_blank.dat'))
    x_dither, y_dither = read_dither(filename)
    npt.assert_array_equal(x_dither, x_ref)
    npt.assert_array_equal(y_dither, y_ref)
    
    # test file with comma
    filename = pkg_resources.resource_filename('kiss', os.path.join("tests/", 'dithering_comma.dat'))
    x_dither, y_dither = read_dither(filename)
    npt.assert_array_equal(x_dither, x_ref)
    npt.assert_array_equal(y_dither, y_ref)
    ###
    return
