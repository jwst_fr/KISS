#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
Created on 15 March 2018
@author: Rene Gastaud

history :

"""

import numpy as np
import numpy.testing as npt
from kiss.kiss import get_det_coord


#

def test_get_det_coord(verbose=True, atol=1E-3):
    vector = (np.arange(7) - 3.) * 10
    dra = vector * 0.11 / 3600 * np.sqrt(2.)
    ddec = vector * 0.11 / 3600 * 2
    p_ra = 20.
    p_dec = 45.
    crpix = np.array([100, 200])
    cdelt = np.array([-0.11, 0.11])
    # test 1
    col, row, w = get_det_coord(dra + p_ra, ddec + p_dec, p_ra, p_dec, crpix, cdelt, 0.)
    diff_x = -col + crpix[0] - vector
    diff_y = (row - crpix[1]) - 2 * vector
    if verbose:
        print('diff_x', diff_x.min(), diff_x.max())
        print('diff_y', diff_y.min(), diff_y.max())
    npt.assert_allclose((-col + crpix[0]), vector, atol=atol)
    npt.assert_allclose((row - crpix[1]), 2 * vector, atol=atol)

    # test 2
    col, row, w = get_det_coord(dra + p_ra, ddec + p_dec, p_ra, p_dec, crpix, cdelt, -90.)
    diff_x = -col + crpix[0] - 2 * vector
    diff_y = (row - crpix[1]) + vector
    if verbose:
        print('diff_x', diff_x.min(), diff_x.max())
        print('diff_y', diff_y.min(), diff_y.max())
    npt.assert_allclose((-col + crpix[0]), 2 * vector, atol=atol)
    npt.assert_allclose((row - crpix[1]), -vector, atol=atol)

    return
