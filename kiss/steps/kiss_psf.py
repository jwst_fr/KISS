from __future__ import print_function

"""
Created on 13 March 2018
@author: Rene Gastaud

history : 

"""
import numpy as np
from .read_cdp import read_psf
import logging

LOG = logging.getLogger('kiss.kiss_psf')

__version__ = 0.1


class KissPSF(object):
    """
    Select the PSF
    """

    def __init__(self, filename):
        """
        Constructor : read the fits file

        :param str fileName: name of the file which contains the coefficients of the polynoms of distortion
        """
        self.filename = filename
        if filename == 'dummy':
            self.create_dummy()
        else:
            psfs, oversampling, row_field, column_field = read_psf(filename)

            # Renormalisation of the PSFs for imager ones. not for coronograph ones because they MUST NOT be renorm.
            # Will only work with imager filters, since coronagraph filters finish with a "C"
            if "W_PSF" in filename:
                psfs = psfs / np.sum(psfs, axis=(1, 2))[:, np.newaxis, np.newaxis]

            self.row_field = row_field
            self.col_field = column_field
            self.psfs = psfs
            self.oversampling = oversampling
        return

    def create_dummy(self):
        """
        Create a dummy psf
        """
        psfs = np.zeros([1, 512, 512])
        psfs[:, 255, 255] = 1.
        self.psfs = psfs
        self.oversampling = 4
        self.row_field = np.array([512])
        self.col_field = np.array([512])
        return

    def get(self, column, row):
        """
        select PSF
        :param float column:  float between 1 and 1024
        :param float row:  float between 1 and 1024
        """
        diff = np.abs(self.row_field - row) + np.abs(self.col_field - column)
        index = diff.argmin()

        my_psf = self.psfs[index]
        return my_psf

    def __str__(self):
        blabla = "kiss psf v{}  filename {}".format(__version__, self.filename)
        return blabla
