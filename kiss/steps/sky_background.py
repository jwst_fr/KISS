#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 26 June 2020
@author: Rene Gastaud
KISS package
history :
     12 oct 2020
"""

import numpy as np
from astropy.io import fits
from astropy import wcs
from astropy import units as u
import logging

LOG = logging.getLogger('kiss.sky_background')

__version__ = 0.1


class skyBackground(object):
    """
    Create a sky background and provide a get method
    """

    def __init__(self, filename, distortion_object, cam_ny=1024, cam_nx=1024):
        """
        Constructor : read the fits file

        :param str fileName: name of the file which contains the coefficients of the polynoms of distortion
        """
        self.cam_nx = cam_nx
        self.cam_ny = cam_ny
        if (distortion_object is not None): self.prepare_coord(distortion_object)
        #
        self.filename = filename
        self.read()
        #
        return

    def prepare_coord(self, distortion_object):
        """
        Compute the distortion takes about 1.5 second
        Depends only on the camera, no need to redo it for each pointng
        """
        self.distortion_object = distortion_object
        y_distorted, x_distorted = np.mgrid[:self.cam_ny, :self.cam_nx]
        x_undist, y_undist = distortion_object.pix2foc(x_distorted.flatten(), y_distorted.flatten())
        x_undist=x_undist.reshape([self.cam_ny, self.cam_nx])
        y_undist=y_undist.reshape([self.cam_ny, self.cam_nx])
        self.x_undist = x_undist
        self.y_undist = y_undist
        return
                                                        

    def read(self, filename=None):
        """
        read the FITS file
        """
        #full_filename = pkg_resources.resource_filename('kiss', self.filename)
        if (filename is not None):
            self.filename = filename
        hdulist = fits.open(self.filename)
        hdu = hdulist[0]
        self.image = hdu.data
        ny, nx = hdu.data.shape
        # and do not forget the header
        header = hdu.header
        self.w = wcs.WCS(header)
        self.w.pixel_shape = (ny, nx)
        self.ny = ny
        self.nx = nx
        self.filter =  header['FILTER']
        self.unit = u.Unit(header['BUNIT'])
        return


    def get(self, cam_wcs):
        """
        project the background to the camera
        :param float column:  float between 1 and 1024
        :param float row:  float between 1 and 1024
        """
        (ra, dec) = cam_wcs.wcs_pix2world(self.x_undist, self.y_undist, 1)
        xs, ys = self.w.wcs_world2pix(ra, dec, 1)
        xsi = np.rint(xs).astype(int)
        ysi = np.rint(ys).astype(int)
        inside_pixels = np.where((xsi >= 0) & (xsi < self.nx) & (ysi >= 0) & (ysi < self.ny))
        image = np.zeros([self.cam_ny, self.cam_nx])
        image[:,:] = np.nan
        image[inside_pixels[0], inside_pixels[1]] = self.image[ysi[inside_pixels], xsi[inside_pixels]]
        nb_missing = self.cam_ny*self.cam_nx - len(inside_pixels[0])
        if (nb_missing > 0): print("number of missing pixels", nb_missing)
        return image

    def compare(self, other):
        """
        compare
        """
        status=True
        diff = self.image - other.image
        if (np.max(np.abs(diff)) > 1e-8):
            print("image different")
            status=False
        # TODO add test w, filter, unit
        return status

    def __str__(self):
        blabla = "skyBackground v{}  filename {}".format(__version__, self.filename)
        blabla += repr(self.w)
        blabla += "\n filter="+self.filter
        blabla += "\n unit="+self.unit.to_string()
        return blabla
