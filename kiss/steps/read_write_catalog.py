#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 26 February 2018  KISS package
# 12 December 2019
#   We have 2 type of input catalog files:
#        simplified: id, ra, dec, flux, wavelength  for historical reason, no more used
#        standard :  id, ra, dec, fluxes for different filters
#   We have 2 types of file format : FITS or csv (ascii text fileà
#   We can read and write both format.
#
#   BEWARE Writing is partial, only one flux for a given filter, TODO all filters
#
#  author R Gastaud  CEA-Saclay-IRFU

import numpy as np
from astropy.io import ascii, fits
from astropy.table import Table, Column
from astropy import units as u
import datetime
import os
import sys
import logging

LOG = logging.getLogger('kiss.read_catalog')

def random_select_catalog(nn,  id0, ra0, dec0, flux0, verbose=False):
    """
    It randomly select a simplified catalog
    
    :param int nn:  the number of selected items
    :param astropy.quantity array ra0  right ascension degree
    :param astropy.quantity array dec0  declination degree
    :param astropy.quantity array flux0 the fluxes in mJy for filter_name
    :param bool verbose
    :return: id, ra, dec, flux
    :rtype: np.array(charray), astropy.quantity array, astropy.quantity array, astropy.quantity array
    """
    big_n = ra0.shape[0]
    if (nn >= big_n):print('error nn > big_n')
    indexes = np.random.randint(0, big_n-1, nn)
    ra   = ra0[indexes]
    dec  = dec0[indexes]
    id   = id0[indexes]
    flux = flux0[indexes]
    if (verbose): print(" total number of items ={}, selected number ={}".format(big_n, nn))
    return id, ra, dec, flux

def extrapolate_flux(cat_flux,  cat_wave, filter_name, verbose=False):
    """
    It extrapolates the flux for the given filter, using Rayleigh-Jeans approximation
    
    :param astropy.quantity flux the fluxes ((usually  in mJy) for  given wavelength
    :param astropy.quantity wavelength (usually in micron) for the given flux
    :param str filter_name:     filter name for MIRIM
    'F560W','F770W', 'F1000W','F1130W','F1280W','F1500W','F1800W','F2100W', 'F2550W'
    
    :return: astropy.quantity flux the fluxes (usually mJy) for filter_name

    """
    filter_wave = (float(filter_name[1:-1]) / 100.)*u.micron
    filter_flux = cat_flux * (cat_wave / filter_wave) ** 2  ## Rayleigh-Jeans
    return filter_flux

#  https://docs.astropy.org/en/stable/io/fits/
def write_catalog_fits_filter(filename, filter_name, id, ra, dec, flux, overwrite=True):
    """
    It writes a simplified catalog with flux for only one filter
    filter name for MIRIM
    'F560W','F770W', 'F1000W','F1130W','F1280W','F1500W','F1800W','F2100W', 'F2550W'
    
    :param str filename:
    :param str filter_name:
    :param np.array(charray) id  the id (name of the star, or just a number)
    :param astropy.quantity array ra  right ascension degree
    :param astropy.quantity dec  declination degree
    :param astropy.quantity flux the fluxes in mJy for filter_name
    :return: 
    """
    col0 = fits.Column(array=id, name='id', format='20A')
    col1 = fits.Column(array=ra.value, name='ra', unit=ra.unit.to_string(), format='E')
    col2 = fits.Column(array=dec.value, name='dec', unit=dec.unit.to_string(), format='E')
    col3 = fits.Column(array=flux.value, name=filter_name, unit=flux.unit.to_string(), format='E')
    hdu = fits.BinTableHDU.from_columns([col0, col1, col2, col3])
    hdu.header['DATE'] = str(datetime.datetime.now())
    hdu.writeto(filename, overwrite=overwrite)
    return

def write_catalog_fits_simple(filename, id, ra, dec, flux, wavelength, overwrite=True):
    """
    It writes a simplified catalog with flux for only one wavelength
    
    :param str filename:
    :param np.array(charray) id  the id (name of the star, or just a number)
    :param astropy.quantity array ra  right ascension degree
    :param astropy.quantity dec  declination degree
    :param astropy.quantity flux the fluxes in mJy for filter_name
    :param astropy.quantity wavelength usually micron
    :return:
    """
    col0 = fits.Column(array=id, name='id', format='20A')
    col1 = fits.Column(array=ra.value, name='ra', unit=ra.unit.to_string(), format='E')
    col2 = fits.Column(array=dec.value, name='dec', unit=dec.unit.to_string(), format='E')
    col3 = fits.Column(array=flux.value, name='flux', unit=flux.unit.to_string(), format='E')
    col4 = fits.Column(array=wavelength.value, name='wavelength', unit=wavelength.unit.to_string(), format='E')
    hdu = fits.BinTableHDU.from_columns([col0, col1, col2, col3, col4])
    hdu.header['DATE'] = str(datetime.datetime.now())
    hdu.writeto(filename, overwrite=overwrite)
    return

def write_catalog_dat_simple(filename, id, ra, dec, flux, wavelength, overwrite=True):
    """
        It writes a simplified catalog with flux for only one wavelength,
        in a file format given by astropy ecsv
        
        :param str filename:
        :param str filter_name:
        :param astropy.quantity array ra  right ascension degree
        :param astropy.quantity dec  declination degree
        :param astropy.quantity flux the fluxes in mJy for wavelength
        :param astropy.quantity wavelength usually micron
        :return:
        """
    col0 = Column(id, name='id')
    col1 = Column(ra, name='ra', unit=ra.unit.to_string(), dtype='float32')
    col2 = Column(dec, name='dec', unit=dec.unit.to_string(), dtype='float32')
    col3 =  Column(flux, name='flux', unit=flux.unit.to_string(), dtype='float32')
    col4 =  Column(wavelength, name='wavelength', unit=wavelength.unit.to_string(), dtype='float32')
    t = Table([col0, col1, col2, col3, col4])
    t.write(filename, format='ascii.ecsv', overwrite=overwrite)
    return

def write_catalog_dat_filter(filename, filter_name, id, ra, dec, flux, overwrite=True):
    """
        It writes a simplified catalog with flux for only one filter
        filter name for MIRIM
        'F560W','F770W', 'F1000W','F1130W','F1280W','F1500W','F1800W','F2100W', 'F2550W'
        The file format  is given by astropy ecsv .
        
        :param str filename:
        :param str filter_name:
        :param astropy.quantity array ra  right ascension degree
        :param astropy.quantity dec  declination degree
        :param astropy.quantity flux the fluxes in mJy for filter_name
        :return:
        """
    col0 = Column(id, name='id')
    col1 = Column(ra, name='ra', unit=ra.unit.to_string(), dtype='float32')
    col2 = Column(dec, name='dec', unit=dec.unit.to_string(), dtype='float32')
    col3 =  Column(flux, name=filter_name, unit=flux.unit.to_string(), dtype='float32')
    t = Table([col0, col1, col2, col3])
    t.write(filename, format='ascii.ecsv', overwrite=overwrite)
    return

def read_catalog_fits(filename, filter_name, verbose=False):
    """
    The input fits file must have column id, ra and dec,
    and fluxes (mJy) for each filter for MIRI Imager 
    'F560W','F770W', 'F1000W','F1130W','F1280W','F1500W','F1800W','F2100W', 'F2550W'
    
    :param str filename: name of the input file.
    :param str filter_name: Given the filter name, will retrieve a specific column with fluxes in mJy for that filter
    :param bool verbose:
    :return: star_id, ra, dec, flux
    :rtype: np.chararray, astropy.quantity, astropy.quantity, astropy.quantity
    """
    hdulist = fits.open(filename)
    table = hdulist[1].data
    star_id = table['id']
    ra = table['ra'] * u.Unit(table.columns['ra'].unit)
    dec =table['dec'] * u.Unit(table.columns['dec'].unit)
    #
    if filter_name in table.names:
        flux = table[filter_name] * u.Unit(table.columns[filter_name].unit)
    else:
        cat_flux = table['flux'] * u.Unit(table.columns['flux'].unit)
        cat_wave = table['wavelength'] * u.Unit(table.columns['wavelength'].unit)
        flux = extrapolate_flux(cat_flux,  cat_wave, filter_name)
    if verbose:
        print(filename, filter_name)
    return star_id, ra, dec, flux

def read_catalog_dat(filename, filter_name, verbose=False):
    """
        Read a star catalog from a .dat file.
        The expected format is ECSV (from astropy)
        doc: http://docs.astropy.org/en/v1.0.9/api/astropy.io.ascii.Ecsv.html
        
        Columns expected are:
        id, ra, dec, filter_name
        The file format  is given by astropy ecsv .

        :param str filename: catalog filename
        :param str filter_name: Given the filter name, will retrieve a specific column with fluxes in mJy for that filter
        :param bool verbose:
        :return: star_id, ra, dec, flux
        :rtype: tuple(np.array(int), np.array(float), np.array(float), np.array(float))

        """
    
    # http://docs.astropy.org/en/stable/units/equivalencies.html
    
    data = ascii.read(filename)
    
    LOG.debug(filename)
    star_id = data["id"].data
    ra = data["ra"].quantity
    dec = data["dec"].quantity
    if (filter_name in data.columns.keys()):
        flux = data[filter_name].quantity
    else:
        cat_flux = data['flux'].quantity
        cat_wave = data['wavelength'].quantity
        flux = extrapolate_flux(cat_flux,  cat_wave, filter_name)
    if verbose:
        print(filename, filter_name)
    return star_id, ra, dec, flux


def read_catalog(filename, filter_name, verbose=True):
    """
    Read the catalog, 
      compute the flux at the nominal wavelength of the filter
      returns the flux and this nominal wavelength


    :param str filename: file containing the catalog of stars

    :param str filter_name: Name of the filter (F1000W for instance)

    :param bool verbose: Display debug info

    :return: (name, ra, dec, flux, wav)
    """

    if not os.path.isfile(filename):
        LOG.error("Catalog '{}' not found".format(filename))
        sys.exit()

    filter_wave = (float(filter_name[1:-1]) / 100.)*u.micron
    #
    # the file can be compressed (.gz added)
    root, ext = os.path.splitext(filename)
    if ext == ".gz":
        ext = os.path.splitext(root)[1]

    LOG.debug("file={} ; ext={}".format(filename, ext))

    if (ext  == '.fits'):
        star_id, ra, dec, filter_flux = read_catalog_fits(filename, filter_name)
    elif(ext == '.dat'):
        star_id, ra, dec, filter_flux = read_catalog_dat(filename, filter_name)
    else:
        LOG.error("Unkown extension {} for catalog".format(ext))
        sys.exit()

    return star_id, ra, dec, filter_flux, filter_wave


#
# The following code is for development and testing only
#
if __name__ == '__main__':
    LOG.info("Testing the fits read catalog routine")
    import time

    #
    filename = 'JA_LMC_MIRI_v2.fits.gz'
    filter_name = 'F1000W'
    t0 = time.time()
    star_id, ra, dec, flux, wav = read_catalog(filename, filter_name, verbose=True)
    t1 = time.time()
    LOG.info('Elapsed time for fits.gz :', t1 - t0, len(star_id))
    LOG.info("\nTest finished.")
    #
    import matplotlib.pyplot as plt

    plt.figure(1)
    plt.semilogy(star_id, flux, '+')
    plt.figure(2)
    plt.plot(ra, dec, '+')
    #
    filename = 'test_catalog.dat'
    filter_name = 'F2000W'
    star_id, ra, dec, flux, wav = read_catalog(filename, filter_name, verbose=True)

    #
