"""
Created on 08 March 2018
@author: Rene Gastaud

history : 

"""
from astropy.io import fits
import numpy as np
import pkg_resources
import os

import logging

LOG = logging.getLogger('kiss.compute_distortion')


__version__ = 0.2


class ComputeDistortion(object):
    """
    Compute the distortion, should be replaced by astropy.wcs.sip
    I centered the coordinates on [511.5, 511.5] , point which is invariant by distortion
    (optical axis center) 
    For this point the first coefficent of the polynoms (constant term ==> shift) is very small.

    reference:
    Shupe, D. L., M. Moshir, J. Li, D. Makovoz and R. Narron. 2005.
    "The SIP Convention for Representing Distortion in FITS Image Headers." ADASS XIV.



    
    """

    def __init__(self, filename):
        """
        Constructor : read the fits file

        :param str fileName: name of the file which contains the coefficients of the polynoms of distortion
        """
        full_filename = pkg_resources.resource_filename('kiss', filename)

        # Consider the file to be local instead of relative to the package
        if not os.path.isfile(full_filename):
            full_filename = filename

        self.filename = full_filename

        hdulist = fits.open(self.filename)
        self.A = hdulist[1].data['A'].squeeze()
        self.B = hdulist[1].data['B'].squeeze()
        self.AP = hdulist[1].data['AP'].squeeze()
        self.BP = hdulist[1].data['BP'].squeeze()
        self.crpix1 = hdulist[1].header['CRPIX1']
        self.crpix2 = hdulist[1].header['CRPIX2']
        self.crota2 = hdulist[1].header['CROTA2']
        hdulist.close()
        
        return

    def foc2pix(self, in_x, in_y):
        """
        focal plane (without distortion) ==> pixel (with distortion)
        
        :param in_x: float array, x position without distortion, in pixel, starting from 0
        :type in_x: array(float)
        :param in_y: float array, y position without distortion, in pixel, starting from 0
        :type in_y: array(float)

        :return: distorded coordinates
        :rtype: (x, y)
        """
        perfect_x = in_x - self.crpix1
        perfect_y = in_y - self.crpix2
        xx = np.zeros(len(perfect_x))
        for i in np.arange(5):
            for j in np.arange(5):
                xx += self.A[i, j] * (perfect_x ** i) * (perfect_y ** j)
                #
        yy = np.zeros(len(perfect_x))
        for i in np.arange(5):
            for j in np.arange(5):
                yy += self.B[i, j] * (perfect_x ** i) * (perfect_y ** j)
        ##
        out_x = xx + self.crpix1
        out_y = yy + self.crpix2
        return out_x, out_y

    def pix2foc(self, in_x, in_y):
        """
        pixel (with distortion) ==>  focal plane (without distortion)

        :param in_x: float array, x position with distortion, in pixel, starting from 0
        :type in_x: array(float)
        :param in_y: float array, y position with distortion, in pixel, starting from 0
        :type in_y: array(float)

        """
        distorted_x = in_x - self.crpix1
        distorted_y = in_y - self.crpix2
        xx = np.zeros(len(distorted_x))
        for i in np.arange(5):
            for j in np.arange(5):
                xx += self.AP[i, j] * (distorted_x ** i) * (distorted_y ** j)
                #
        yy = np.zeros(len(distorted_y))
        for i in np.arange(5):
            for j in np.arange(5):
                yy += self.BP[i, j] * (distorted_x ** i) * (distorted_y ** j)
        ##
        out_x = xx + self.crpix1
        out_y = yy + self.crpix2
        return out_x, out_y

    def __str__(self):
        blabla = "Compute_distortion v{} with polynoms:{} crpix1:{} crpix2:{} crota2:{}".format(__version__, self.filename, self.crpix1, self.crpix2, self.crota2)
        
        return blabla
