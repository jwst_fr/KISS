#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 26 February 2018  KISS package
#  author R Gastaud

from astropy.io import ascii, fits
from astropy import units as u
import os
import sys
import logging

LOG = logging.getLogger('kiss.read_catalog')


def read_catalog_fits_gz(filename, filter_name, verbose=False):
    """
    The input fits.gz file must have a column per filter for MIRI Imager, with fluxes in mJy. column id, ra and dec
    must also exists

    :param str filename:
    :param str filter_name: Given the filter name, will retrieve a specific column with fluxes in mJy for that filter
    :param bool verbose:
    :return: star_id, ra, dec, flux
    :rtype: tuple(np.array(int), np.array(float), np.array(float), np.array(float))
    """
    hdulist = fits.open(filename)
    star_id = hdulist[1].data['id']
    ra = hdulist[1].data['ra'] * u.Unit(hdulist[1].columns['ra'].unit)
    dec = hdulist[1].data['dec'] * u.Unit(hdulist[1].columns['dec'].unit)
    #
    flux = hdulist[1].data[filter_name] * u.Unit(hdulist[1].columns[filter_name].unit)
    if verbose:
        print(filename, filter_name)
    return star_id, ra, dec, flux


def read_catalog_fits(filename):
    """
    Read the catalog provided by Alistair and moved into a .fits.gz by Rene

    The pointing is used to compute the pixel coordinates of each star
    Select the flux of the star in the given filter

    :param str filename: file containing the catalog of stars

    :return: (name, ra, dec, flux, wavelength)
    :rtype: tuple(np.array(int), np.array(float), np.array(float), np.array(float), np.array(float))
    """

    hdulist = fits.open(filename)
    star_id = hdulist[1].data['id']
    ra = hdulist[1].data['ra'] * u.Unit(hdulist[1].columns['ra'].unit)
    dec = hdulist[1].data['dec'] * u.Unit(hdulist[1].columns['dec'].unit)
    flux = hdulist[1].data['flux'] * u.Unit(hdulist[1].columns['flux'].unit)
    wav = hdulist[1].data['wavelength'] * u.Unit(hdulist[1].columns['wavelength'].unit)

    LOG.debug(filename)

    return star_id, ra, dec, flux, wav


def read_catalog_dat(filename):
    """
    Read a star catalog from a .dat file.
    The expected format is ECSV (from astropy)
    doc: http://docs.astropy.org/en/v1.0.9/api/astropy.io.ascii.Ecsv.html

    Columns expected are:
    id ra dec wavelength flux

    :param str filename: catalog filename

    :return: ja_id ,ra, dec, flux
    :rtype: tuple(np.array(int), np.array(float), np.array(float), np.array(float), np.array(float))
    """

    # http://docs.astropy.org/en/stable/units/equivalencies.html

    data = ascii.read(filename)

    LOG.debug(filename)

    return data["id"], data["ra"], data["dec"], data["flux"], data["wavelength"]


def read_catalog(filename, filter_name, verbose=True):
    """
    Read the catalog, 
      compute the flux at the nominal wavelength of the filter
      returns the flux and this nominal wavelength


    :param str filename: file containing the catalog of stars

    :param str filter_name: Name of the filter (F1000W for instance)

    :param bool verbose: Display debug info

    :return: (name, ra, dec, flux, wav)
    """

    if not os.path.isfile(filename):
        LOG.error("Catalog '{}' not found".format(filename))
        sys.exit()

    filter_wav = float(filter_name[1:-1]) / 100.
    #
    # .fits.gz is a special case
    root, ext = os.path.splitext(filename)
    if ext == ".gz":
        ext = os.path.splitext(root)[1] + ext

    LOG.debug("file={} ; ext={}".format(filename, ext))

    my_functions = {".fits": read_catalog_fits, ".dat": read_catalog_dat}

    if ext in my_functions.keys():
        star_id, ra, dec, cat_flux, cat_wav = my_functions[ext](filename)
        filter_flux = cat_flux * (cat_wav / filter_wav) ** 2
    elif ext == '.fits.gz':
        star_id, ra, dec, filter_flux = read_catalog_fits_gz(filename, filter_name, verbose=verbose)
    else:
        LOG.error("Unkown extension {} for catalog".format(ext))
        sys.exit()

    return star_id, ra, dec, filter_flux, filter_wav


#
# The following code is for development and testing only
#
if __name__ == '__main__':
    LOG.info("Testing the fits read catalog routine")
    import time

    #
    filename = 'JA_LMC_MIRI_v2.fits.gz'
    filter_name = 'F1000W'
    t0 = time.time()
    star_id, ra, dec, flux, wav = read_catalog(filename, filter_name, verbose=True)
    t1 = time.time()
    LOG.info('Elapsed time for fits.gz :', t1 - t0, len(star_id))
    LOG.info("\nTest finished.")
    #
    import matplotlib.pyplot as plt

    plt.figure(1)
    plt.semilogy(star_id, flux, '+')
    plt.figure(2)
    plt.plot(ra, dec, '+')
    #
    filename = 'test_catalog.dat'
    filter_name = 'F2000W'
    star_id, ra, dec, flux, wav = read_catalog(filename, filter_name, verbose=True)

    #
