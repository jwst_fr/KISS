#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 26 February 2018  KISS package
#  author R Gastaud

"""
    This function reads a file for a given filename in the cdp directory then return the flat.
    The flat can be :
     a sky flat (Low Frequency Flat), pixel flat (High Frequency Flat) or the total Falt.
    It assumes that you have system variable CDP_DIR which gives the directory where the fits files are stored.
    ( ls $CDP_DIR to check)
    The format should be the same than cdp
    """

import glob
import os
import sys

from astropy.io import fits
import numpy as np
import logging

LOG = logging.getLogger('kiss.read_cdp')

try:
    cdp_dir = os.environ['CDP_DIR']
except KeyError:
    LOG.error("CDP_DIR environment variable not set")
    sys.exit()


def get_cdp(filename):
    """

    :param str filename: CDP filename

    :return: hdulist of the corresponding .fits file
    """
    LOG.debug("Reading CDP {}".format(filename))
    # February 22nd 2021  4.0.2
    original_dir = os.path.dirname(filename)
    if(len(original_dir) > 0):
        cdp_file = filename
    else:
        cdp_file = os.path.join(cdp_dir, filename)

    if not os.path.isfile(cdp_file):
        LOG.error(f"get_cdp: No files found for filename='{filename}' in CDP_DIR='{cdp_dir}'")
        sys.exit()

    hdulist = fits.open(cdp_file)
    return hdulist


def read_flat(filename):
    """
        A simple python function to read psf from a fits file.

        :param str filename: string, name of the file MIRI_FM_MIRIMAGE_F1500W_SKYFLAT_06.00.00.fits
                     MIRI_FM_MIRIMAGE_FAST_F1000W_PIXELFLAT_05.01.00.fits

        :return nd.array: flat image or 1. if filename is none
        """
    if filename == 'none':
        return 1.
    hdulist = get_cdp(filename)
    hdu = hdulist['SCI']
    flat = hdu.data

    return flat


def read_pce(filename):
    """
        A simple python function to read psf from a fits file.

        :param str filename: string, name of the PCE CDP
        """

    hdulist = get_cdp(filename)

    wavelength = hdulist['DATA_TABLE'].data['WAVELENGTH'].squeeze()
    efficiency = hdulist['DATA_TABLE'].data['EFFICIENCY'].squeeze()

    ll = len(wavelength)
    ii = np.where(efficiency > efficiency.max() / 1e4)

    wavelength = wavelength[ii]
    efficiency = efficiency[ii]

    LOG.debug("read_pce: Filter length before: {}, after: {}".format(ll, len(wavelength)))

    return wavelength, efficiency


def read_psf(filename):
    """
    A simple python function to read psf from a fits file.

    :param str filename: PSF filename

    :return: psfs, oversampling, col_field, row_field
        psfs: the psfs for this wavelength at different position in the detector
        oversampling: the oversampling of the psf, the psf is magnified in regard of the detector
        col_field: column position of each psf in the detector, unit pixel
        row_field: row position of each psf in the detector, unit pixel
    :rtype: Union[nd.array(float, size=(npsfs, ny, nx), float, nd.array(float, size=(npsfs), float, nd.array(float, size=(npsfs)]

    """
    hdulist = get_cdp(filename)

    hdu = hdulist['SCI']
    psfs = hdu.data

    # Special case if there's only one PSF in the file:
    if len(psfs.shape) == 2:
        psfs = psfs[np.newaxis, :, :]

    hdu = hdulist['PSF_LUT']

    col_field = hdu.data['COL_FIELD']
    row_field = hdu.data['ROW_FIELD']

    pixel_size = hdulist[0].header['PIXSIZE']
    oversampling = 25 / pixel_size

    return psfs, oversampling, col_field, row_field


#
# The following code is for development and testing only
#
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm

    print("Testing the simple mirim read flat routine")
    PLOTTING = True  # Set to False to turn off plotting.
    filename = 'MIRI_FM_MIRIMAGE_FAST_F1000W_PIXELFLAT_05.01.00.fits'
    flat = read_flat(filename)
    print("min = {}, max = {}".format(flat.min(), flat.max()))
    if PLOTTING:
        plt.figure(1)
        plt.title("{} FLAT".format(filename))
        plt.imshow(flat, norm=LogNorm(), origin='lower', cmap='gist_stern')
        plt.colorbar(orientation='horizontal')
        # plt.savefig('first_psf_10microns.png')
        plt.show()

    print("\nTest finished.")

    print("Testing the simple mirim read pce routine")
    PLOTTING = True  # Set to False to turn off plotting.
    filename = "MIRI_FM_MIRIMAGE_F1000W_PCE_06.00.00.fits"
    wavelength, efficiency = read_pce(filename)
    print("wavelength max=", wavelength[efficiency.argmax()], "efficiency max=", efficiency.max())
    if PLOTTING:
        plt.figure(1)
        plt.title("{} PCE".format(filename))
        plt.plot(wavelength, efficiency)
        # plt.savefig('first_psf_10microns.png')
        plt.show()

    print("\nTest finished.")

    print("Testing the simple mirim read psf routine")
    PLOTTING = True  # Set to False to turn off plotting.
    filename = "MIRI_FM_MIRIMAGE_F1000W_PSF_05.02.00.fits"
    my_psf, oversampling, columns, rows = read_psf(filename)
    print("oversampling={}".format(oversampling))
    if PLOTTING:
        plt.figure(1)
        plt.title("{} PSF".format(filename))
        plt.imshow(my_psf, norm=LogNorm(), origin='lower', cmap='gist_stern')
        plt.colorbar(orientation='horizontal')
        # plt.savefig('first_psf_10microns.png')
        plt.show()

    print("\nTest finished.")
