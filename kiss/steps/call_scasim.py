#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Rene Gastaud 14 March 2018
#  /Users/gastaud/scratch/CARs/CAP202F

import numpy as np

from miri.datamodels.sim import MiriIlluminationModel
from miri.simulators.scasim.sensor_chip_assembly import SensorChipAssembly3
from miri.simulators.scasim import __version__

import logging

LOG = logging.getLogger('kiss.call_scasim')


def call_scasim(image, configuration, out_file):
    """
    Just an overlay calling mirsim scasim.
    This is the only part which import mirisim package
    
    :param image: the image, unit electron/second
    :type image: numpy array of floats, shape 1024, 1024
    
    :param configuration: contains parameters
    :type configuration: dict
   
    :param out_file: name of the output file
    :type out_file: str
    
    Return  none
    
     """
    filtername = configuration['processing']['filtername']
    wave = float(filtername[1:-1]) / 100.  # see catalog
    waves = np.repeat(wave, 1024 * 1024).reshape([1024, 1024])
    illum_model = MiriIlluminationModel(intensity=image, wavelength=waves)
    illum_model.set_instrument_metadata('MIRIMAGE')
    illum_model.meta.intensity.units = u'electron/s'

    sca = SensorChipAssembly3()  # MIRIMAGE
    nexposures = configuration['noise']['exposures']
    nints = configuration['noise']['integrations']
    ngroups = configuration['noise']['frames']

    det_image = sca.simulate_pipe(illum_model, readout_mode='FAST', ngroups=ngroups, nints=nints, verbose=3,
                                  simulate_poisson_noise=True, simulate_read_noise=True,
                                  simulate_ref_pixels=True, simulate_bad_pixels=True,
                                  simulate_dark_current=True, simulate_flat_field=True,
                                  simulate_gain=True, simulate_nonlinearity=False,
                                  simulate_drifts=False, simulate_latency=False, seedvalue=1,
                                  qe_adjust=False)  # qe_adjust see steven beard email

    det_image.write(out_file)
    return

# cosmic_ray_mode='NONe, include_pixeldq=False, include_groupdq=False,
# miri.datamodels.miri_exposure_model.MiriExposureModel
