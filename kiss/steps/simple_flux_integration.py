#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 26 February 2018  KISS package
#  author R Gastaud
"""
    This function integrates the flux
    """

import logging

import scipy.integrate
from astropy import constants
# http://docs.astropy.org/en/stable/units/equivalencies.html
from astropy import units as u

from .read_cdp import read_pce

LOG = logging.getLogger('kiss.simple_flux_integration')


def get_flux_prefactor(filename):
    """
    Get flux prefactor (for an input flux of 1). To retrieve the real flux Fd on the detector in Electron/s
    one only has to multiply this prefactor P with the following formulae:
    Fd = P * Fs * (Ls/L0)**2
    where Fs is the star flux at the wavelength Ls. L0 is the refence wavelength for the prefactor computation.

    In the Rayleigh-Jeans approximations, the fluxes are defined by:
    B_nu(L) = B_0 x (L0/L)^2

    The spectrum shape is the same for all stars within a scaling factor
    We compute the conversion factor for an input flux of 1 (whatever is the input unity, milliJansky or else)
    Then, for each star, we just have to multiply its flux with
    this prefactor to have the total flux of the star in Electron/s


    :param str filename: CDP filename for the PCE FITS

    :return: (P, L0)
        flux F in Electron/s for a reference wavelength L0
        This flux F is equivalent of an input flux of 1 at L0
        (if input fluxes are in milliJansky, the input reference flux at L0 is 1 milliJansky)
    """
    cel = constants.c

    telescope_area = 25. * u.m * u.m
    telescope_transmission = 0.88  # to check

    wavelength, efficiency = read_pce(filename)

    flux_unit2 = u.photon / u.m ** 2 / u.s / u.Hz
    flux_unit1 = u.milliJansky

    wavelength_unit = u.micron
    wavelength = wavelength * wavelength_unit
    wavelength0 = wavelength[efficiency.argmax()]

    # Rayleigh–Jeans law
    flux1 = efficiency * flux_unit1 * (wavelength0 / wavelength) ** 2
    flux2 = flux1.to(flux_unit2, equivalencies=u.spectral_density(wavelength))

    frequencies = cel / wavelength
    frequencies = frequencies.decompose()
    flux_total = - scipy.integrate.trapz(flux2, frequencies)  # in photon/m^2/s

    # By default astropy.unity doesn't simply the Hz through integration.
    # We force it with decompose. We translate from ph / (Hz m2 s2) to ph / (m2 s)
    flux_total = flux_total.decompose()

    flux_total = flux_total * telescope_area * telescope_transmission

    # Uses the initial unit for the reference flux, to ensure that unity
    # convert to the correct value while retrieving the star flux
    flux_prefactor = flux_total / flux_unit1
    return flux_prefactor, wavelength0


#
# The following code is for development and testing only
#
if __name__ == '__main__':
    print("Testing the get_flux_prefactor routine")
    filterName = 'F1000W'  # not used
    filename = 'MIRI_FM_MIRIMAGE_F1000W_PCE_06.00.00.fits'
    #  ??? ref_flux = 24484.4  # electron/second
    ref_flux = 26800.  # electron/second from PASP IX page 692
    flux_total = get_flux_prefactor(filename)
    rdiff = (flux_total[0].value - ref_flux) / ref_flux
    rdiff2 = round(rdiff * 100, 2)
    print("Relative difference to the official simulator {:.2f} %".format(rdiff2))
    #  rdif  1.6 %
