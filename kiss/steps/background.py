#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 26 February 2018  KISS package
#  author R Gastaud
import logging

LOG = logging.getLogger('kiss.background')


def get_background(filter_name):
    """
        A simple python function to get background

        Background values here are hardcoded, but comes
        from the "HIGH" background option of MIRISim
        
        :param str filter_name: name of the filter, e.g. 'F560W'

        Return
        ~~~~~~
        Background in e-/s
        """

    # Electron/s
    bkg = {'F560W': 4.807974,
           'F770W': 35.172371,
           'F1000W': 64.150711,
           'F1130W': 25.07362,
           'F1280W': 114.73563,
           'F1500W': 308.9093,
           'F1800W': 939.67957,
           'F2100W': 3627.843,
           'F2550W': 6477.57}

    value = bkg[filter_name]

    LOG.debug("For {}, Background = {:.2f} e-/s".format(filter_name, value))
    return value


#
# The following code is for development and testing only
#
if __name__ == '__main__':
    print("Testing the simple mirim read psf routine")
    PLOTTING = True  # Set to False to turn off plotting.
    filterName = 'F1000W'
    background = get_background(filterName)
    print("background={} for {}".format(background, filter))

    print("\nTest finished.")
