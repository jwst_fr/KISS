from astropy.io import fits
from scipy import ndimage as nd
from scipy.ndimage.measurements import center_of_mass
from miricap import imager as imlib
import time
import numpy as np
import os
from functools import partial
import multiprocessing


def read_psf(filename):
    with fits.open(filename) as hdulist:
        image = hdulist[1].data[0]


    return image


def find_position(image):

    y, x = center_of_mass(image)

    return x, y


def ndshift3(image, dx, dy):
    """

    :param image:
    :param dx:
    :param dy:
    :return:
    """

    shifted_image = nd.shift(image, [dy, dx], order=3, mode="constant", cval=0.)

    return shifted_image


def ndshift1(image, dx, dy):
    """

    :param image:
    :param dx:
    :param dy:
    :return:
    """

    shifted_image = nd.shift(image, [dy, dx], order=1, mode="constant", cval=0.)

    return shifted_image


def fftshift(image, dx, dy):
    """

    :param image:
    :param dx:
    :param dy:
    :return:
    """

    shifted_image = imlib.im.subpixel_shift(image, dy, dx)

    return shifted_image


def statistical_study(data, data_name=None):
    """
    Compare statistical properties of the input data

    :param data: array of values

    :return:
    """

    if data_name is None:
        data_name = "Data"

    msg = ""

    data_mean = np.mean(data)
    data_std = np.std(data)

    # msg += f"{data_name}: {len(data)} elements.\n"
    msg += f"{data_name} = {data_mean:.4f} +- {data_std:.4f}"

    return msg


def generate_dithers(n):
    """
    Generate random sub pixel shift around 0.

    :param n: Number of dither positions required
    :return: x and y shifts in pixels
    :rtype: tuple(x_shifts, y_shifts)
    """
    tmp = np.random.RandomState().uniform(0, 1, (2, n))


    return tuple(tmp)


def shift_accuracy(image, func, ref_x_dithers, ref_y_dithers):
    """
    Test the accuracy of the shifting function with the image and dithers provided

    :param image: image that will be shifted
    :param callable func: function that shift position, function need to support the following call:
                 shifted = func(image, dx, dy)
    :param ref_x_dithers: dither shifts in pixels for X axis
    :type ref_x_dithers: nd.array(dx)
    :param ref_y_dithers: dither shifts in pixels for Y axis
    :type ref_y_dithers: nd.array(dy)
    :return:
    """

    print(f"Test shift accuracy with {func.__name__}")

    if isinstance(ref_x_dithers, float):
        ref_x_dithers = np.asarray([ref_x_dithers])

    if isinstance(ref_y_dithers, float):
        ref_y_dithers = np.asarray([ref_y_dithers])

    nb_dithers = len(ref_x_dithers)

    ref_x, ref_y = find_position(image)
    ref_flux = image.sum()

    x_dither_errors = []  # absolute error
    y_dither_errors = []  # absolute error
    flux_errors = []  # percentage
    nb_negative_pixels = []
    start_time = time.time()
    i = 0

    for (dx, dy) in zip(ref_x_dithers, ref_y_dithers):
        i += 1
        shifted_image = func(image, dx, dy)

        nb_negative = np.sum(shifted_image < 0)

        pos_x, pos_y = find_position(shifted_image)

        flux_errors.append((shifted_image.sum() - ref_flux) / ref_flux)
        x_dither_errors.append(pos_x - ref_x - dx)
        y_dither_errors.append(pos_y - ref_y - dy)
        nb_negative_pixels.append(nb_negative)

        percentage = i / nb_dithers * 100
        stop_time = time.time()
        eta = (nb_dithers - i) * (stop_time - start_time) / i
        print(f"\rPosition {i}/{nb_dithers} ({percentage:.1f}%) ETA {eta:.0f} s", end="")

    total_time = stop_time - start_time
    avg_time = total_time / nb_dithers
    print(f"\nDone in {total_time:.0f} s ({avg_time} s per dither)")

    return ref_x_dithers, ref_y_dithers, flux_errors, x_dither_errors, y_dither_errors, nb_negative_pixels

multi = False
nb_position = 24
filename = "gaussian_psf.fits"

x_dithers, y_dithers = generate_dithers(nb_position)

# Create chuncks of arrays for multiprocess (because the overhead is huge and become negligible if chuncks are
# processed rather than individual dithers
nb_proc = multiprocessing.cpu_count()
x_chuncks = np.array_split(x_dithers, min(nb_proc, nb_position))
y_chuncks = np.array_split(y_dithers, min(nb_proc, nb_position))

chuncks = [it for it in zip(x_chuncks, y_chuncks)]

data_filename = f"gauss_dither_positions.dat"
if not os.path.isfile(data_filename):
    header_line = "x dither (pixel), y dither (pixel)"
else:
    header_line = ''

of = open(data_filename, "a")
np.savetxt(of, np.column_stack([x_dithers, y_dithers]),
           header=header_line, delimiter=",", fmt='%.5f')
of.close()

psf = read_psf(filename)

for func in [ndshift3, ndshift1, fftshift]:
    poolfunc = partial(shift_accuracy, psf, func)
    # Used inside the pool worker, due to partial, the initial name is lost if not manually set
    poolfunc.__name__ = func.__name__

    t_start = time.time()
    if multi:
        pool = multiprocessing.Pool()
        print(f"Pool Starting with {pool._processes} processes and {poolfunc.__name__}.")
        result = pool.starmap(poolfunc, chuncks)
    else:
        result = []
        for c in chuncks:
            res = poolfunc(*c)
            result.append(res)

    # result will have shape (nb_proc, 6, nb_elem_in_chuncks), so we need to reshape it
    result = np.concatenate(result, axis=1)  # This make the first dim as list of arrays merged along their 2nd axis
    result = result.T # We want first axis to be column id, and second be line index (6, n)

    duration = time.time() - t_start
    print(f"Calculation done in {duration:.1f}s")

    data_filename = f"gauss_shift_accuracy_{poolfunc.__name__}.dat"
    print(f" Write {data_filename}")
    if not os.path.isfile(data_filename):
        header_line = "original x dither (pixel), original y dither (pixel), flux error (%), x dither error (pixel), " \
                      "y dither error (pixel), nb_negative_pixels"
    else:
        header_line = ''

    # Write/append to data file (this might contain the reference value and or previous statistics for the same filter)
    of = open(data_filename, "a")
    np.savetxt(of, result, #np.column_stack([flux_errors, x_dither_errors, y_dither_errors, nb_negative_pixels]),
               header=header_line, delimiter=",", fmt=('%f', '%f', '%e', '%e', '%e', '%d'))
    of.close()



# plt.show()
