#!/bin/bash
#SBATCH -N1  # 1 complete node
#SBATCH --exclusive
#SBATCH --mem 500GB
#SBATCH --job-name=capkissgauss
#SBATCH --time=6-00:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=christophe.cossou@cea.fr # send-to address

echo "nodename: $SLURM_NODELIST"
source /data/cluster/JWST/activate_mirisim.sh

which python

#/data/cluster/JWST/anaconda3/bin/python compute_center_precision.py
/data/cluster/JWST/anaconda3/envs/miricle.devel/bin/python compute_gauss_shift_accuracy_parallel.py
