from astropy.io import fits
from scipy import ndimage as nd
import photutils
from miricap import imager as imlib
import matplotlib.pyplot as plt
import time
import numpy as np
import os


def read_psf(filename):
    with fits.open(filename) as hdulist:
        image = hdulist[1].data[0]


    return image


def find_position(image):
    """

    :param image:
    :return: center position (x, y)
    """
    # parameter to set the absolute image value above which to select sources in the star finding algorithm.
    # The level is defined by sf_threshold*bkg_sigma
    threshhold = 0.1

    star_border = 20

    # Dictionary for the filter fwhm size in pixels
    fwhm = 2.00  # for F560W


    # Keep only 10 brightest sources. Avoid sources at the edge (problem to retrieve the PSF there)
    dsf = photutils.DAOStarFinder(threshold=threshhold, fwhm=fwhm, brightest=10,
                                  exclude_border=True, sigma_radius=2 * star_border)
    sources = dsf(image)

    # Sort by brightest stars first
    sources.sort("flux", reverse=True)

    # Only take brigthest source for now
    center = (sources["xcentroid"][0], sources["ycentroid"][0])

    return center


def ndshift3(image, dx, dy):
    """

    :param image:
    :param dx:
    :param dy:
    :return:
    """

    shifted_image = nd.shift(image, [dy, dx], order=3, mode="constant", cval=0.)

    return shifted_image


def ndshift1(image, dx, dy):
    """

    :param image:
    :param dx:
    :param dy:
    :return:
    """

    shifted_image = nd.shift(image, [dy, dx], order=1, mode="constant", cval=0.)

    return shifted_image


def fftshift(image, dx, dy):
    """

    :param image:
    :param dx:
    :param dy:
    :return:
    """

    shifted_image = imlib.im.subpixel_shift(image, dy, dx)

    return shifted_image


def statistical_study(data, data_name=None):
    """
    Compare statistical properties of the input data

    :param data: array of values

    :return:
    """

    if data_name is None:
        data_name = "Data"

    msg = ""

    data_mean = np.mean(data)
    data_std = np.std(data)

    # msg += f"{data_name}: {len(data)} elements.\n"
    msg += f"{data_name} = {data_mean:.4f} +- {data_std:.4f}"

    return msg


def generate_dithers(n):
    """
    Generate random sub pixel shift around 0.

    :param n: Number of dither positions required
    :return: x and y shifts in pixels
    :rtype: tuple(x_shifts, y_shifts)
    """
    tmp = np.random.RandomState().uniform(0, 1, (2, n))

    return tuple(tmp)


def test_shift_accuracy(image, func, dithers):
    """
    Test the accuracy of the shifting function with the image and dithers provided

    :param image: image that will be shifted
    :param callable func: function that shift position, function need to support the following call:
                 shifted = func(image, dx, dy)
    :param dithers: shifts in pixels for multiples dither positions
    :type dithers: tuple(nd.array(dx), nd>array(dy)0
    :return:
    """

    print(f"Test shift accuracy with {func.__name__}")

    ref_x, ref_y = find_position(image)
    ref_flux = image.sum()

    ref_x_dithers, ref_y_dithers = dithers

    x_dither_errors = []  # absolute error
    y_dither_errors = []  # absolute error
    flux_errors = []  # percentage
    nb_negative_pixels = []
    start_time = time.time()
    i = 0
    nb_dithers = len(ref_x_dithers)
    nb_negative = 0
    for (dx, dy) in zip(ref_x_dithers, ref_y_dithers):
        i += 1
        shifted_image = func(image, dx, dy)

        nb_negative = np.sum(shifted_image < 0)

        pos_x, pos_y = find_position(shifted_image)

        flux_errors.append((shifted_image.sum() - ref_flux) / ref_flux)
        x_dither_errors.append(pos_x - ref_x)
        y_dither_errors.append(pos_y - ref_y)
        nb_negative_pixels.append(nb_negative)

        percentage = i / nb_dithers * 100
        stop_time = time.time()
        eta = (nb_dithers - i) * (stop_time - start_time) / i
        print(f"\rPosition {i}/{nb_dithers} ({percentage:.1f}%) ETA {eta:.0f} s", end="")

    total_time = stop_time - start_time
    avg_time = total_time / nb_dithers
    print(f"\nDone in {total_time:.0f} s ({avg_time} s per dither)")



    print(f"For shift function: {func.__name__}")
    nb_problems = nb_dithers - nb_negative_pixels.count(0)  # if count is not 0, we have a problem of negative pixels
    neg_percent = nb_problems / nb_dithers * 100
    print(f"{nb_problems}/{nb_dithers} had negative values ({neg_percent}%)")

    print(statistical_study(flux_errors, data_name="Flux error (%)"))
    print(statistical_study(x_dither_errors, data_name="X dither error (pixels)"))
    print(statistical_study(y_dither_errors, data_name="Y dither error (pixels)"))

    # fig1 = imlib.plot.histogram(flux_errors, xlabel="Flux error (%)", title=f" shifts using {func.__name__}")
    # fig2 = imlib.plot.histogram(x_dither_errors, xlabel="X dither error [pixel]", title=f" shifts using {func.__name__}")
    # fig3 = imlib.plot.histogram(y_dither_errors, xlabel="Y dither error [pixel]", title=f" shifts using {func.__name__}")
    #
    # filename_prefix = f"{func.__name__}"
    #
    # file1 = f"{filename_prefix}_flux_error_dist.pdf"
    # file2 = f"{filename_prefix}_xshift_error_dist.pdf"
    # file3 = f"{filename_prefix}_yshift_error_dist.pdf"
    #
    # fig1.savefig(file1)
    # fig2.savefig(file2)
    # fig3.savefig(file3)

    data_filename = f"shift_accuracy_{func.__name__}.dat"
    if not os.path.isfile(data_filename):
        header_line = "flux error (%), x dither error (pixel), y dither error (pixel), nb_negative_pixels"
    else:
        header_line = ''

    # Write/append to data file (this might contain the reference value and or previous statistics for the same filter)
    of = open(data_filename, "a")
    np.savetxt(of, np.column_stack([flux_errors, x_dither_errors, y_dither_errors, nb_negative_pixels]),
               header=header_line, delimiter=",", fmt=('%.7f','%.7f', '%.7f', '%d'))
    of.close()


nb_position = 3
filename = "MIRI_FM_MIRIMAGE_F560W_PSF_07.02.00.fits"
micro_pattern_dx = [0.000, 0.015, 0.033, 0.054, 0.000, 0.015, 0.033, 0.054, 0.000, 0.015, 0.033, 0.054, 0.000, 0.015, 0.033, 0.054]
micro_pattern_dy = [0.000, 0.000, 0.000, 0.000, 0.015, 0.015, 0.015, 0.015, 0.033, 0.033, 0.033, 0.033, 0.054, 0.054, 0.054, 0.054]

x_dithers, y_dithers = generate_dithers(nb_position)

data_filename = f"dither_positions.dat"
if not os.path.isfile(data_filename):
    header_line = "x dither, y dither (pixel)"
else:
    header_line = ''

of = open(data_filename, "a")
np.savetxt(of, np.column_stack([x_dithers, y_dithers]),
           header=header_line, delimiter=",", fmt='%.5f')
of.close()

psf = read_psf(filename)

test_shift_accuracy(psf, ndshift3, [x_dithers, y_dithers])
test_shift_accuracy(psf, ndshift1, [x_dithers, y_dithers])
test_shift_accuracy(psf, fftshift, [x_dithers, y_dithers])

# plt.show()
