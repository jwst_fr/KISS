from astropy.modeling import models
import matplotlib.pyplot as plt
import numpy as np
from miricap import imager as imlib

x_size, y_size = (512, 512)
width = 30

my_model = models.Gaussian2D(x_mean=x_size/2, y_mean=y_size/2, x_stddev=width, y_stddev=width)
x_sample = range(x_size)
y_sample = range(y_size)

x, y = np.meshgrid(x_sample, y_sample)
image = my_model(x, y)

psfs = np.repeat(image[np.newaxis, :, :], 9, axis=0)

imlib.write.write_jwst_fits(psfs, filename="gaussian_psf.fits")

# fig, ax = plt.subplots()
# ax.imshow(image)
# plt.show()
