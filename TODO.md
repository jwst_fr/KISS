# TODO

1. use the different psfs and not only the psf at the center of the detector !
2. add the possibility to call scasim (create ramp and add pixel flat, cosmics, etc..)
3. add a new format of catalog (.dat) text file containing:
star Id, RA, Dec, flux, wavelenght
4. add  flags to turn on/off each step
5. add in the fits file the history (name of the program,version of the program, name of the user, name and value of important parameters)
6. documentation (docstring)
7. unit tests
8. global tests with simple cases (one star in the center, one in each corner ..., different fluxes and fliters)  ==> 
   create a dummy catalog of 5 stars, one at the center, the other in the corners, with 	different fluxes to recognize them
9. add dithering = read APT pointing table


not urgent

replace the function 'create_distortion_polynoms' by astropy fits wcs SIP.
use  https://github.com/STScI-MIRI/miricoord ?
