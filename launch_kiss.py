#!/usr/bin/env python
# -*- coding: utf-8 -*-

from kiss.kiss import kiss
import time
import sys
import os

if len(sys.argv) > 1:
    configname = sys.argv[1]
else:
    tmp = 'configuration.ini'
    if os.path.isfile(tmp):
        configname = tmp
    else:
        print("Error: You need to provide an .ini filename as argument of the script")
        sys.exit()

plot = False

t0 = time.time()
image = kiss(configname)
t1 = time.time()

print('Elapsed time for kiss: {:.2f} s'.format(t1 - t0))

if plot:
    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm

    plt.figure(1)
    plt.title("Simulation")
    plt.imshow(image, norm=LogNorm(), origin='lower', cmap='gist_stern')
    plt.colorbar(orientation='horizontal')
    plt.savefig('simulation.pdf')
    plt.show()
